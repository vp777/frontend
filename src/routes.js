import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout } from "./layouts";

// Route Views
import BlogOverview from "./views/BlogOverview";
import UserProfileLite from "./views/UserProfileLite";
import AddNewPost from "./views/AddNewPost";
import Errors from "./views/Errors";
import ComponentsOverview from "./views/ComponentsOverview";
import Tables from "./views/Tables";
import BlogPosts from "./views/BlogPosts";
import Teacher from "./components/Teacher/Teacher";
import Subject from "./components/Subject/Subject";
import Class from "./components/Class/Class";
import Subscriber from './components/Subscriber/Subscriber';
import TimeTable from "./components/TimeTable/TimeTable";
import Room from './components/Rooms/Room';
import TeacherTimeTable from './components/TeacherTimeTable/TeacherTimeTable';
import ViewTeacher from "./components/Teacher/ViewTeacherStats";
import GenerateTimeTable from './components/GenerateTimeTable/GenerateTimeTable';
import User from './components/User/User';

import Login from './components/Login/Login';

import login_data from './stores/loginStore';

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/login" />
  },
  {
    path : "/viewteacher",
    layout : DefaultLayout,
    component : ViewTeacher
  },
  {
    path : '/login',
    layout : DefaultLayout,
    component : Login
  },
  {
    path : '/logout',
    layout : DefaultLayout,
    component : () => {login_data.logout();return <Redirect to="/login" />}
  },  
  {
    path : '/teacher',
    layout : DefaultLayout,
    component : Teacher
  },
  {
    path : '/timetable',
    layout : DefaultLayout,
    component : TimeTable
  },
  {
    path : '/room',
    layout : DefaultLayout,
    component : Room
  },
  {
    path : '/teachertimetable',
    layout : DefaultLayout,
    component : TeacherTimeTable
  },
  {
    path : '/subject',
    layout : DefaultLayout,
    component : Subject
  },
  {
    path : '/subscriber',
    layout : DefaultLayout,
    component : Subscriber
  },
  {
    path : '/gentimetable',
    layout : DefaultLayout,
    component : GenerateTimeTable
  },
  {
    path : '/class',
    layout : DefaultLayout,
    component : Class
  },
  {
    path : '/user',
    layout : DefaultLayout,
    component : User
  },
  {
    path: "/dashboard",
    layout: DefaultLayout,
    component: BlogOverview
  },
  {
    path: "/user-profile-lite",
    layout: DefaultLayout,
    component: UserProfileLite
  },
  {
    path: "/add-new-post",
    layout: DefaultLayout,
    component: AddNewPost
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  {
    path: "/components-overview",
    layout: DefaultLayout,
    component: ComponentsOverview
  },
  {
    path: "/tables",
    layout: DefaultLayout,
    component: Tables
  },
  {
    path: "/blog-posts",
    layout: DefaultLayout,
    component: BlogPosts
  }
];
