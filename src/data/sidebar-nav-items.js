
export default function() {
  return [
    /*{
      title: " Dashboard",
      to: "/dashboard",
      htmlBefore: '<i class="material-icons">edit</i>',
      htmlAfter: ""
    },*/
    {
      title: "Teacher",
      htmlBefore: '<i class="material-icons">assignment_ind</i>',
      to: "/teacher",
    },
    {
      title: "Subject",
      htmlBefore: '<i class="material-icons">book</i>',
      to: "/subject",
    },
    {
      title: "Class",
      htmlBefore: '<i class="material-icons">group</i>',
      to: "/class",
    },
    {
      title: "Room",
      htmlBefore: '<i class="material-icons">room</i>',
      to: "/room",
    },
    {
      title: "Generate TimeTable",
      htmlBefore: '<i class="material-icons">build</i>',
      to: "/gentimetable",
    },
    {
      title: "TimeTable",
      htmlBefore: '<i class="material-icons">calendar_today</i>',
      to: "/timetable",
    },
    {
      title: "Teacher Time Table",
      htmlBefore: '<i class="material-icons">assessment</i>',
      to: "/teachertimetable",
    },
    {
      title: "Users",
      htmlBefore: '<i class="material-icons">supervised_user_circle</i>',
      to: "/user",
    },
    {
      title: "Logout",
      htmlBefore: '<i class="material-icons">logout</i>',
      to: "/logout",
    },
    
    
    /*
    {
      title: "Blog Posts",
      htmlBefore: '<i class="material-icons">vertical_split</i>',
      to: "/blog-posts",
    },
    {
      title: "Add New Post",
      htmlBefore: '<i class="material-icons">note_add</i>',
      to: "/add-new-post",
    },
    {
      title: "Forms & Components",
      htmlBefore: '<i class="material-icons">view_module</i>',
      to: "/components-overview",
    },
    {
      title: "Tables",
      htmlBefore: '<i class="material-icons">table_chart</i>',
      to: "/tables",
    },
    {
      title: "User Profile",
      htmlBefore: '<i class="material-icons">person</i>',
      to: "/user-profile-lite",
    },
    {
      title: "Errors",
      htmlBefore: '<i class="material-icons">error</i>',
      to: "/errors",
    }*/
  ];
}
