import {store} from 'react-easy-state';
import alert from './Alert';
import host from './host';
import login_data from './loginStore';
import databaseSelectionStore from '../components/databaseSelectionStore';
const request = require('request');


const gen_time_data = store({
        isLoading : false,


        genTimeTable(data){
            gen_time_data.isLoading = true; 
            request(host+'genTimeTable?data='+data+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
                console.error('error:', error); // Print the error if one occurred
                if(error){
                    alert.showMessage(error.message,'error');
                    return;
                }
                console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                console.log('body:', body); // Print the HTML for the Google homepage.
                
                try{

                    var data = JSON.parse(body);
                }
                catch(err){
                    alert.showMessage('Invalid Reponse','error');
                    return;
                }

                if(!error){
                    if(data.status==='success'){
                           alert.showMessage("Time Table Generated Successfully",'success');
                    }
                    else{
                        console.log(data.status);
                        alert.showMessage(data.message,'error');
                    }
                }
                gen_time_data.isLoading = false;
            });
    
        },

})

export default gen_time_data;