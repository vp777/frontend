import {store} from 'react-easy-state';
import alert_data from './Alert';
import searchData from './searchData';
import host from './host';
import login_data from './loginStore';
import databaseSelectionStore from '../components/databaseSelectionStore';
const request = require('request');

const teacher_data = store({
    data : [],
    viewData : [],
    isLoading : false,
    hasFetchError : false,

    openEdit : false,

    //to add and data
    name : null,
    phone : null,
    email : null,
    authorized_periods : 0,

    id : null,

    //for search
    search_attribute : 'name',

    setEditTeacher(data){
        console.log("EDit teacher",data);
        teacher_data.name = data.name;
        teacher_data.phone = data.phone;
        teacher_data.email = data.email;
        teacher_data.id = data.tid;
        teacher_data.authorized_periods = data.authorized_periods;
        teacher_data.openEdit = true;
    },

    closeEditTeacher(clear){
        teacher_data.openEdit = false;
        if(clear){
            teacher_data.name = null;
            teacher_data.phone = null;
            teacher_data.email = null;
            teacher_data.authorized_periods = null;
        }
    },

    editTeacher(){

        teacher_data.closeEditTeacher(false);
        teacher_data.isLoading = true;


        request(host+'editTeacher?name='+teacher_data.name
                    +'&phone='+teacher_data.phone
                    +'&email='+teacher_data.email
                    +'&id='+teacher_data.id
                    +"&departmentName="+databaseSelectionStore.selectedDepartment
                    +login_data.getAuthString(), function (error, response, body) {
            if(error){
                alert_data.showMessage(error.message,'danger');
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='success'){
                    alert_data.showMessage(data.message,'success');
                    teacher_data.getData();    
                }
                else{
                    alert_data.showMessage(data.message,'danger');
                }
            }

            teacher_data.isLoading = false;
        })

        teacher_data.name = null;
        teacher_data.phone = null;
        teacher_data.email = null;
        teacher_data.id    = null;
        teacher_data.authorized_periods = null;

    },


    search(name){
        console.log(name);
        console.log(this.search_attribute);
        teacher_data.isLoading = true;
        teacher_data.viewData = searchData(teacher_data.data,teacher_data.search_attribute,name);
        teacher_data.isLoading = false;
    },

    getData(){
        console.log("Getting Teacher Data");
        teacher_data.isLoading = true;
        teacher_data.hasFetchError = false;
        request(host+'getData?name=Teachers&orderby=tid'+login_data.getAuthString()
                    +"&departmentName="+databaseSelectionStore.selectedDepartment, 
                    function (error, response, body) {
            if(error){
                alert_data.showMessage(error.message,'danger');
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='failure'){
                    alert_data.showMessage(data.message,'danger'); 
                    teacher_data.isLoading = false; 
                    teacher_data.hasFetchError = true;
                    return;  
                }
                teacher_data.viewData = data.data;
                teacher_data.data = data.data;    
            }

            teacher_data.isLoading = false;
        })
    },

    removeTeacher(id){
        var result = window.confirm("Are you sure?");
        if (!result) {
                return;
        }
        teacher_data.isLoading = true;
        request(host+'removeTeacher?tid='+id+login_data.getAuthString()+
                    "&departmentName="+databaseSelectionStore.selectedDepartment, 
            function (error, response, body) {
            if(error){
                alert_data.showMessage(error.message,'danger');
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='success'){
                    alert_data.showMessage(data.message,'success');
                    teacher_data.getData();    
                }
                else{
                    alert_data.showMessage(data.message,'danger');
                }
            }

            teacher_data.isLoading = false;
        })
    },

    addTeacher(){
        console.log("Add Teacher Initiated");
        var name = teacher_data.name;
        var email = teacher_data.email;
        var phone = teacher_data.phone;
        var authorized_lectures = teacher_data.authorized_periods;

        if(name===null || email===null || phone===null){
            alert_data.showMessage("Please fill all Details ","warning");
            return;
        }

        if(isNaN(authorized_lectures)){
            console.log("authrozized lectures = ",authorized_lectures);
            alert_data.showMessage('Please enter valid number for authorized lectures',"warning");
            return;
        }

        teacher_data.isLoading = true;
        request(host+'addTeacher?name='+name
                                +"&phone="+phone
                                +"&email="+email
                                +"&authorized_lectures="+authorized_lectures
                                +"&departmentName="+databaseSelectionStore.selectedDepartment
                                +login_data.getAuthString(),
         function (error, response, body) {
                console.error('error:', error); // Print the error if one occurred
                if(error){
                    alert_data.showMessage(error.message,'error');
                }
                console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                console.log('body:', body); // Print the HTML for the Google homepage.
                var data = JSON.parse(body);
                if(!error){
                    if(data.status==='success'){
                           alert_data.showMessage("Teacher Added Succesfully",'success');
                           teacher_data.getData();
                    }
                    else{
                        console.log(data.status);
                        alert_data.showMessage(data.message,'danger');
                    }
                }
                teacher_data.isLoading = false;
                teacher_data.name = null;
                teacher_data.phone = null;
                teacher_data.email = null;
                teacher_data.authorized_periods = null;
        });
    },
})


export default teacher_data;