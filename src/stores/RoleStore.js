import {store} from 'react-easy-state';
import alert from './Alert';
import host from './host';
import login_data from './loginStore';
import databaseSelectionStore from '../components/databaseSelectionStore';
const request = require('request');


const role_data = store({
    data : [],

    getData(){
        request(host+'getData?name=roles&orderby=role_id'+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert.showMessage(error.message,'danger');
                return;
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='failure'){
                    alert.showMessage(data.message,'danger');    
                }
                role_data.data = data.data;    
            }
        })
    }
})

export default role_data;