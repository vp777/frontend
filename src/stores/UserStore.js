import {store} from 'react-easy-state';
import alert from './Alert';
import host from './host';
import login_data from './loginStore';
import databaseSelectionStore from '../components/databaseSelectionStore';
const request = require('request');


const user_data = store({
    isLoading : false,
    data : [],

    user_name : undefined,
    user_id : undefined,
    password : undefined,
    confirm_password : undefined,
    role_id : undefined,

    getData(){
        user_data.isLoading = true;
        request(host+'getUser?'+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert.showMessage(error.message,'danger');
                return;
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='failure'){
                    alert.showMessage(data.message,'danger');
                    return;    
                }
                user_data.data = data.data;    
                console.log(user_data.data);
            }

            user_data.isLoading = false;
        })
    },

    addUser(){
        if(user_data.password!==user_data.confirm_password && (user_data.password!==undefined)){
            alert.showMessage('Password Doesnt Match','danger');
            return;
        }
        if(user_data.password===undefined){
            alert.showMessage("Please Enter Password",'danger');
            return;
        }
        if(user_data.role_id===undefined){
            alert.showMessage('Please Select Role','danger');
            return;
        }
        if(user_data.user_name===undefined || user_data.user_id===undefined){
            alert.showMessage('Please Enter Valid User details','danger');
        }

        user_data.isLoading = true;
        request(host+'addUser?uname='+user_data.user_name
                    +'&add_user_id='+user_data.user_id
                    +'&password='+user_data.password
                    +'&role_id='+user_data.role_id
                    +"&departmentName="+databaseSelectionStore.selectedDepartment
                    +login_data.getAuthString(), 
        function (error, response, body) {
            console.error('error:', error); // Print the error if one occurred
            if(error){
                alert.showMessage(error.message,'danger');
                user_data.isLoading = false;
                return;
            }
            console.log('statusCode:', response && response.statusCode); 
            console.log('body:', body); 
            var data = JSON.parse(body);
            if(!error){
                if(data.status==='success'){
                       alert.showMessage("User Added Succesfully",'success');
                       user_data.getData();
                }
                else{
                    console.log(data.status);
                    alert.showMessage(data.message,'danger');
                }
            }
            user_data.isLoading = false;
        });

    },

    delUser(user_id){
        var confirm = window.confirm('Are you sure?');
        if(!confirm){
            return;
        }
        user_data.isLoading = true;
        request(host+'delUser?user_id_to_del='+user_id+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert.showMessage(error.message,'danger');
                return;
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='success'){
                    alert.showMessage(data.message,'success');
                    user_data.getData();    
                }
                else{
                    alert.showMessage(data.message,'danger');
                }
            }
            user_data.isLoading = false;
        })
    }
})

export default user_data;