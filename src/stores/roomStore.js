import {store} from 'react-easy-state';
import searchData from './searchData';
import alert from './Alert';
import host from './host';
import login_data from './loginStore';
import databaseSelectionStore from '../components/databaseSelectionStore';
const request = require('request');



const room_data = store({
    room_no : null,
    room_name : null,
    isLoading : false,

    search_attribute : 'room_name',

    viewData : [],
    data : [],

    setRoomNo(no){
        room_data.room_no = no;
    },
    setRoomName(name){
        room_data.room_name = name;
    },

    search(name){
        room_data.isLoading = true;
        room_data.viewData = searchData(room_data.data,room_data.search_attribute,name);
        room_data.isLoading = false;
    },

    addRoom(){
        const no = room_data.room_no;
        const name = room_data.room_name;
        if(no===null || name===null){
            alert.showMessage('Enter valid Details.','warning');
            return;
        }
        room_data.isLoading = true;
        request(host+'addRoom?no='+no+"&name="+name+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            console.error('error:', error); // Print the error if one occurred
            if(error){
                alert.showMessage(error.message,'error');
            }
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.
            var data = JSON.parse(body);
            if(!error){
                if(data.status==='success'){
                       alert.showMessage("Room Added Succesfully",'success');
                       room_data.getRoomData();
                }
                else{
                    console.log(data.status);
                    alert.showMessage(data.message,'danger');
                }
            }
            room_data.isLoading = false;
            room_data.no = null;
            room_data.name = null;
        });

    },
    getRoomData(){
        room_data.isLoading = true;
        request(host+'getData?name=room&orderby=room_no'+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert.showMessage(error.message,'danger');
                return;
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='failure'){
                    alert.showMessage(data.message,'danger');    
                }
                room_data.viewData = data.data;
                room_data.data = data.data;    
            }

            room_data.isLoading = false;
        })
    },
    removeRoomData(no){
        var result = window.confirm("Are you sure?");
        if (!result) {
                return;
        }
        room_data.isLoading = true;
        request(host+'removeRoom?no='+no+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert.showMessage(error.message,'danger');
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='success'){
                    alert.showMessage(data.message,'success');
                    room_data.getRoomData();    
                }
                else{
                    alert.showMessage(data.message,'danger');
                }
            }

            room_data.isLoading = false;
        })
    },
})


export default room_data;