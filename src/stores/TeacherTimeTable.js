import {store} from 'react-easy-state';
import alert from './Alert';
import host from './host';
import login_data from './loginStore';
import databaseSelectionStore from '../components/databaseSelectionStore';
const request = require('request');


const ttdata = store({
    isLoading : false,

    teacher_id : undefined,
    day : 'monday',

    day_data : [],

    setTeacherId(teacher_id){
        ttdata.teacher_id = teacher_id;
        if(teacher_id!==undefined){
            ttdata.getData();
        }
    },

    setDay(day){
        ttdata.day = day;
        ttdata.getData();
    },

    getData(){
        ttdata.isLoading = true;
        if(ttdata.teacher_id===undefined){
            alert.showMessage("Kindly Select A Lecturer");
            return;
        }
        ttdata.day_data = [];
        request(host+'getTeacherTimeTable?teacher_id='+ttdata.teacher_id+"&day="+ttdata.day+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            console.error('error:', error); // Print the error if one occurred
            if(error){
                alert.showMessage(error.message,'error');
                ttdata.isLoading = false;
                return;
            }
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); 
            try{
                var data = JSON.parse(body);
            }
            catch(error){
                console.log(error);
                alert.showMessage(error.message);
                ttdata.isLoading = false;
                return;
            }
            if(!error){
                if(data.status==='success'){
                        ttdata.day_data = data.data;
                        console.log(ttdata.day_data);
                }
                else{
                    console.log(data.status);
                    alert.showMessage(data.message,'danger');
                }
            }
           ttdata.isLoading = false;
        });
    }
})

export default ttdata;