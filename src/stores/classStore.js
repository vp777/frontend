import {store} from 'react-easy-state';
import alert_data from './Alert';
import searchData from './searchData';
import host from './host';
import login_data from './loginStore';
import databaseSelectionStore from '../components/databaseSelectionStore';
const request = require('request');

const class_data = store({
    class_name : null, //both for create and delete
    class_strength : null,
    isLoading : false,
    viewData : [],
    data : [],

    setClassName(class_name){
        class_data.class_name = class_name;
    },

    setClassStrength(n){
        class_data.class_strength = parseInt(n);
    },

    search(name){
        console.log(name);
        class_data.isLoading = true;
        class_data.viewData = searchData(class_data.data,'class_name',name);
        class_data.isLoading = false;
    },
    
    addClass(){
        console.log("Add Class Initiated");
        if(class_data.class_name===null || class_data.class_name===undefined){
            window.alert('Kindly Enter Class Name');
            return;
        }

        class_data.isLoading = true;
        console.log("Requesting Add Class for name "+class_data.class_name);
        request(host+'addClass?name='+class_data.class_name+login_data.getAuthString()+
                     "&class_strength="+(class_data.class_strength)+
                     "&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
                console.error('error:', error); // Print the error if one occurred
                if(error){
                    alert_data.showMessage(error.message,'error');
                }
                console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                console.log('body:', body); // Print the HTML for the Google homepage.
                var data = JSON.parse(body);
                if(!error){
                    if(data.status==='success'){
                           alert_data.showMessage("Class Added Succesfully",'success');
                           class_data.getClassData();
                    }
                    else{
                        console.log(data.status);
                        alert_data.showMessage(data.message,'danger');
                    }
                }
                class_data.isLoading = false;
                class_data.class_name = null;
        });
    },


    getClassData(){
        class_data.isLoading = true;
        request(host+'getData?name=class&orderby=class_id'+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert_data.showMessage(error.message,'danger');
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='failure'){
                    alert_data.showMessage(data.message,'danger');    
                }
                class_data.viewData = data.data;
                class_data.data = data.data;   
            }

            class_data.isLoading = false;
        })
    },

    removeClassData(id){
        var result = window.confirm("Are you sure?");
        if (!result) {
                return;
        }
        class_data.isLoading = true;
        request(host+'removeClass?class_id='+id+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert_data.showMessage(error.message,'danger');
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='success'){
                    alert_data.showMessage(data.message,'success');
                    class_data.getClassData();    
                }
                else{
                    alert_data.showMessage(data.message,'danger');
                }
            }

            class_data.isLoading = false;
        })
    },

    modifyClass(id,classname){
        var new_class_name = prompt("Please enter new Class Name", classname);
        console.log(new_class_name);
        if(new_class_name===null){
            //when user cancels
            return;
        }
        class_data.isLoading = true;
        request(host+'modifyClass?class_id='+id+'&class_name='+new_class_name+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert_data.showMessage(error.message,'danger');
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='success'){
                    alert_data.showMessage(data.message,'success');
                    class_data.getClassData();    
                }
                else{
                    alert_data.showMessage(data.message,'danger');
                }
            }

            class_data.isLoading = false;
        })
    },




})


export default class_data;