import {store} from 'react-easy-state';
import alert from './Alert';
import host from './host';
import login_data from './loginStore';
import databaseSelectionStore from '../components/databaseSelectionStore';
const request = require('request');

const timetable_data = store({
    isLoading : false,

    day : 'monday',
    class_id : undefined,
    day_table : [],

    setClassID(class_id){
        timetable_data.class_id = class_id;
        if(class_id!==undefined){
            timetable_data.getTimeTableData();
        }
    },

    setDay(day){
        timetable_data.day = day;
        timetable_data.getTimeTableData();
    },

    getTimeTableData(){
        timetable_data.isLoading = true;
        if(timetable_data.class_id==undefined){
            alert.showMessage('Kindly Select Class Name');
            timetable_data.isLoading = false;   
            return;
        }
        timetable_data.day_table = [];
        request(host+'getClassTimeTable?class_id='+timetable_data.class_id+"&day="+timetable_data.day+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            console.error('error:', error); // Print the error if one occurred
            if(error){
                alert.showMessage(error.message,'error');
                timetable_data.isLoading = false;
                return;
            }
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); 
            try{
                var data = JSON.parse(body);
            }
            catch(error){
                console.log(error);
                alert.showMessage(error.message);
                timetable_data.isLoading = false;
                return;
            }
            if(!error){
                if(data.status==='success'){
                        timetable_data.day_table = data.data;
                }
                else{
                    console.log(data.status);
                    alert.showMessage(data.message,'danger');
                }
            }
           timetable_data.isLoading = false;
        });
    },
})

export default timetable_data;