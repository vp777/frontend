import {store} from 'react-easy-state';
import searchData from './searchData';
import alert from './Alert';
import host from './host';
import login_data from './loginStore';
import databaseSelectionStore from '../components/databaseSelectionStore';
const request = require('request');

const sub_data = store({
    subject_id : null,
    subject_name : null,
    no_of_classes : null,
    duration_of_class : null,
    class_id : null,
    teacher_id : null,
    room_id : null,
    subject_type : "theory",

    data : [],
    viewData : [],

    search_attribute : 'subject_name',

    isLoading : false,

    resetData(){
        sub_data.subject_id = null;
        sub_data.subject_name = null;
        sub_data.no_of_classes = null;
        sub_data.duration_of_class = null;
        sub_data.class_id = null;
        sub_data.teacher_id = null;
        sub_data.room_id = null;
        sub_data.subject_type = "theory";
    },

    addSubject(){
        sub_data.isLoading = true;
        request(host+'addSubject?id='+sub_data.subject_id
                +"&name="+sub_data.subject_name
                +"&no_of_classes="+sub_data.no_of_classes
                +"&duration="+sub_data.duration_of_class
                +"&class_id="+sub_data.class_id
                +"&teacher_id="+sub_data.teacher_id
                +"&subject_type="+sub_data.subject_type
                +"&departmentName="+databaseSelectionStore.selectedDepartment
                +"&room_id="+sub_data.room_id+login_data.getAuthString(), 
        function (error, response, body) {
            console.error('error:', error); // Print the error if one occurred
            if(error){
                alert.showMessage(error.message,'error');
            }
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.
            var data = JSON.parse(body);
            if(!error){
                if(data.status==='success'){
                       alert.showMessage("Subject Added Succesfully",'success');
                       sub_data.getData();
                }
                else{
                    console.log(data.status);
                    alert.showMessage(data.message,'danger');
                }
            }
            sub_data.isLoading = false;
            sub_data.resetData();
        });

    },

    search(name){
        sub_data.isLoading = true;
        sub_data.viewData = searchData(sub_data.data,sub_data.search_attribute,name);
        sub_data.isLoading = false;
    },

    getData(){
        sub_data.isLoading = true;
        request(host+'getData?name=subjects&orderby=subject_id'+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert.showMessage(error.message,'danger');
                return;
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='failure'){
                    alert.showMessage(data.message,'danger');  
                    return;  
                }
                sub_data.viewData = data.data;
                sub_data.data = data.data;    
            }

            sub_data.isLoading = false;
        })
    },

    removeSubject(id){
        var result = window.confirm("Are you sure?");
        if (!result) {
                return;
        }
        sub_data.isLoading = true;
        request(host+'removeSubject?id='+id+login_data.getAuthString()+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert.showMessage(error.message,'danger');
            }
            else{
                var data = JSON.parse(body);
                if(data.status==='success'){
                    alert.showMessage(data.message,'success');
                    sub_data.getData();    
                }
                else{
                    alert.showMessage(data.message,'danger');
                }
            }
            sub_data.isLoading = false;
        })
    }
    
})

export default sub_data;