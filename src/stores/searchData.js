//1.data is the set where the key is to searched
//2.attribute is the one identifies the data
//3.key is the value that is to searched

var searchkey = null;

export default function search(data,attribute,key){
    if(is_empty_string(key) || data.length===0){
        return data;
    }
    if(is_empty_string(attribute)){
        return data;
    }
    searchkey = attribute;
    var temp = [];
    data.forEach(function(data){
        if(data[attribute].includes(key)){
            temp.push(data);
        }
    })
    temp.sort(compare);
    return temp;
}   



function compare( a, b ) {
      if ( a[searchkey] < b[searchkey] ){
            return -1;
      }
      if ( a[searchkey] > b[searchkey] ){
            return 1;
      }
      return 0;
}

function is_empty_string(x){
           return ( 
                (typeof x == 'undefined')
                            ||
                (x == null) 
                            ||
                (x == false)  //same as: !x
                            ||
                (x.length == 0)
                            ||
                (x == "")
                            ||
                (x.replace(/\s/g,"") == "")
                            ||
                (!/[^\s]/.test(x))
                            ||
                (/^\s*$/.test(x))
              );
}