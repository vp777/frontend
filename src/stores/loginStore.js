import {store} from 'react-easy-state';
import alert from './Alert';
import host from './host';
import databaseSelectionStore from '../components/databaseSelectionStore';
const request = require('request');


const login_data = store({
    isLoggedIn : false,
    isLoading : false,

    jwtToken : undefined,

    user_id : undefined,
    password : undefined,

    getUserID(){
        return login_data.user_id;
    },

    getToken(){
        return login_data.jwtToken;
    },

    getAuthString(){
        return '&user_id='+login_data.user_id+"&token="+login_data.jwtToken;
    },

    setUserName(uname){
        login_data.user_id = uname;
    },

    setPassword(password){
        login_data.password = password;
    },

    login(){
        var uname = login_data.user_id;
        var password = login_data.password;

        if(uname===undefined || password===undefined){
            alert.showMessage('Please Enter Valid Credentials','danger');
            return;
        }
        if(!databaseSelectionStore.selectedDepartment){
            alert.showMessage("Please select a department");
            return;
        }
        login_data.isLoading = true;
        request(host+'login?username='+uname+'&password='+password+"&departmentName="+databaseSelectionStore.selectedDepartment, function (error, response, body) {
            if(error){
                alert.showMessage(error.message,'danger');
                login_data.isLoading = false;
                return;
            }
            else{
                var res_data = JSON.parse(body);
                if(res_data.status==='failure'){
                    alert.showMessage(res_data.message,'danger');
                    login_data.isLoggedIn = false; 
                    login_data.isLoading = true;
                    return;   
                }
                login_data.jwtToken = res_data.message; 
                console.log("JWT Token = ",login_data.jwtToken);
                login_data.isLoggedIn = true;
                login_data.isLoading = false;
            }
        })

    },
    logout(){
        login_data.jwtToken = undefined;
        login_data.isLoggedIn = false;
        console.log(login_data);
    }
})


export default login_data;