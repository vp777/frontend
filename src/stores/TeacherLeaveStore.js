import {store} from 'react-easy-state';
import alert from './Alert';
import host from './host';
import databaseSelectionStore from '../components/databaseSelectionStore';
import login_data from './loginStore';
const request = require('request');

const teacherLeaveStore = store({

    isLoading : false,

    start_date : null,
    end_date : null,
    teacher_leave_data : [],
    
    addTeacherLeaves(teacher_id,start_date,end_date){
        if(!teacher_id){
            alert.showMessage("Teacher id is not valid ","danger");
            return;
        }
        if(!start_date){
            alert.showMessage("Start date is not valid","danger");
            return;
        }
        if(!end_date){
            alert.showMessage("end date is not valid","danger");
            return;
        }

        teacherLeaveStore.isLoading =true;

        start_date = new Date(start_date);
        end_date   = new Date(end_date);

        var start_date_string = (start_date.getFullYear()+"-"+(start_date.getMonth()+1)+"-"+start_date.getDate());
        var end_date_string = (end_date.getFullYear()+"-"+(end_date.getMonth()+1)+"-"+end_date.getDate());

        const url = `${host}addTeacherLeave?teacher_id=${teacher_id}&start_date=${start_date_string}&end_date=${end_date_string}&departmentName=${databaseSelectionStore.selectedDepartment}${login_data.getAuthString()}`;


        request(url,function(err,response,body){
            if(err){
                alert.showMessage(err.message,"danger");
                teacherLeaveStore.isLoading = false
                return;
            }
            
            if(response.statusCode!==200){
                alert.showMessage("Failed","danger");
                teacherLeaveStore.isLoading = false;
                return;
            }
            else{
                var res_data = JSON.parse(body);
                if(res_data.status==="failure"){
                    alert.showMessage(res_data.message,"danger");
                    teacherLeaveStore.isLoading = false;
                    return;
                }
                alert.showMessage('Leave added succesfully',"success");
                teacherLeaveStore.isLoading = false;
                teacherLeaveStore.getTeacherLeaves(teacher_id);
                return;
            }
        })

    },

    getTeacherLeaves(teacher_id){
        if(!teacher_id){
            alert.showMessage("Teacher id is not valid","danger");
            return;
        }
        teacherLeaveStore.isLoading = true;
        teacherLeaveStore.teacher_leave_data = [];
        const url = `${host}getTeacherLeaves?teacher_id=${teacher_id}&departmentName=${databaseSelectionStore.selectedDepartment}${login_data.getAuthString()}`;

        request(url,function(err,res,body){
            if(err){
                alert.showMessage(err.message,"danger");
                teacherLeaveStore.isLoading = false
                return;
            }
            else{
                var res_data = JSON.parse(body);
                if(res_data.status==="failure"){
                    alert.showMessage(res_data.message,"danger");
                    teacherLeaveStore.isLoading = false;
                    return;
                }
                else{
                    teacherLeaveStore.teacher_leave_data = res_data["data"];
                    teacherLeaveStore.isLoading = false;
                    return;
                }
            }
        })
    }

})

export default teacherLeaveStore;