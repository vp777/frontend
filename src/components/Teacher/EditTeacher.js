import React from 'react';
import {  Modal, ModalBody, ModalHeader,Col,Button} from "shards-react";
import {view} from 'react-easy-state';
import teacher_data from '../../stores/TeacherStore';
import Input from '../common/Input';

export default view((props)=>{
    var isLoading = teacher_data.isLoading;
    var openEdit  = teacher_data.openEdit;
    var data = {
        name : teacher_data.name,
        email : teacher_data.email,
        phone : teacher_data.phone,
    }
    console.log(openEdit);
    return <EditTeacher isLoading={isLoading} data={data} open={openEdit} close={props.close} />
})

function setName(name){
  teacher_data.name = name; 
}

function setPhone(phone){
  teacher_data.phone = phone;
}

function setEmail(email){
  teacher_data.email = email;
}

function editTeacher(){
  //teacher_data.aTeacher();
  console.log("ABC");
}

class EditTeacher extends React.Component{
        constructor(props) {
            super(props);
            this.state = { open: false };
            this.toggle = this.toggle.bind(this);
        }

        componentDidMount(){
            this.setState({open:this.props.open});
        }

        componentDidUpdate(prevProps){
            if(prevProps.open!==this.props.open){
                this.setState({open:this.props.open});
            }
        }
    
      toggle() {
        this.setState({
          open: !this.state.open
        });
        teacher_data.closeEditTeacher(true);
      }
    
    render(){
        const {open} = this.state;
        return(
         <div>
            <Modal open={open} toggle={this.toggle}>
              <ModalHeader>
                    <b>Edit Teacher</b>     
            </ModalHeader>
              <ModalBody>
                <Input size="8" defaultValue={this.props.data.name} name="Name" type="text" onChange={(evt)=>{setName(evt.target.value)}} />
                <Input size="8" defaultValue={this.props.data.phone} name="Phone" type="text" onChange={(evt)=>{setPhone(evt.target.value)}} />
                <Input size="8" defaultValue={this.props.data.email} name="Email" type="text" onChange={(evt)=>{setEmail(evt.target.value)}} />
               
                <Col size="8" className={"pt-3"}>
                    <Button theme="primary" onClick={()=>{teacher_data.editTeacher();this.toggle();}}>Update Teacher</Button>
                </Col>
              </ModalBody>
            </Modal>
          </div>
        )
    }
}
