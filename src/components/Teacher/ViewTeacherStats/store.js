import {store, view} from 'react-easy-state';
import alert from './../../../stores/Alert';
import host from '../../../stores/host';
import databaseSelectionStore from '../../databaseSelectionStore';
const request = require('request');

const viewTeacherStore = store({

    isLoading : false,

    tid : null,
    
    centralLV : 0,
    centralPeriods : 0,

    techLV : 0,
    techPeriods : 0,


    examLV : 0,
    examPeriods : 0,

    coiLV : 0,
    coiPeriods : 0,

    total_subjects : 0,
    completed_periods : 0,
    completed_percentage : 0,

    completed_pds : 0,
    theory_pds : 0 ,
    practical_pds : 0 ,

    teacher_subjects:  [],

    getTeacherSubjects(tid){
        
        const url = `${host}getTeacherSubjects?teacher_id=${tid}&departmentName=${databaseSelectionStore.selectedDepartment}`;

        request.get(url,function(err,res,body){
            if(err){
                alert.showMessage(err,"danger");
                return;
            }

            var result = JSON.parse(body);
            console.log(result);

            if(result["status"]==="success"){
                viewTeacherStore.teacher_subjects = result["data"];
                return;
            }
            else{
                viewTeacherStore.teacher_subjects = [];
                return;
            }
        })
        
    },


    getTeacherMisc(tid){
        if(!tid){
            alert.showMessage("TID cannot be empty","danger");
            return;
        }
        viewTeacherStore.getTeacherSubjects(tid);
        viewTeacherStore.tid = tid;
        const url = `${host}getMisc?tid=${tid}&departmentName=${databaseSelectionStore.selectedDepartment}`;
        
        viewTeacherStore.isLoading = true;

        request.get(url,function(err,res,body){
            
            viewTeacherStore.isLoading = false;
            if(err){
                alert.showMessage(err,"danger");
                return;
            }

            var result = JSON.parse(body);
            console.log("result = ",result);

            if(result["status"]==="success"){
                viewTeacherStore.centralLV = result.miscData["central_lv"];
                viewTeacherStore.centralPeriods = result.miscData["central_periods"];
                viewTeacherStore.techLV = result.miscData["tech_lv"];
                viewTeacherStore.techPeriods = result.miscData["tech_periods"];
                viewTeacherStore.examLV = result.miscData["exam_lv"];
                viewTeacherStore.examPeriods = result.miscData["exam_periods"];
                viewTeacherStore.coiLV = result.miscData["coi_lv"];
                viewTeacherStore.coiPeriods = result.miscData["coi_periods"];
                viewTeacherStore.theory_pds = result["totalTheory"];
                viewTeacherStore.practical_pds = result["totalPractical"];
                viewTeacherStore.completed_pds = result["completedPDS"];
                viewTeacherStore.total_subjects = result.subject_count;
                console.log("Completed PDs = ",result["completedPDS"]);
                return;
            }
            else{
                alert.showMessage(res["message"],"danger");
                return;
            }
            
        })
    },


    updateMisc(){
        if(!viewTeacherStore.tid){
            console.log("Invalid tid = ",viewTeacherStore.tid);
            alert.showMessage("Tid is not valid","danger");
            return;
        }
        const url = `${host}updateMisc?tid=${viewTeacherStore.tid}`+
                                      `&central_lv=${viewTeacherStore.centralLV}`+
                                      `&central_periods=${viewTeacherStore.centralPeriods}`+
                                      `&tech_lv=${viewTeacherStore.techLV}`+
                                      `&tech_periods=${viewTeacherStore.techPeriods}`+
                                      `&exam_lv=${viewTeacherStore.examLV}`+
                                      `&exam_periods=${viewTeacherStore.examPeriods}`+
                                      `&coi_lv=${viewTeacherStore.coiLV}`+
                                      `&coi_periods=${viewTeacherStore.coiPeriods}`+
                                      `&departmentName=${databaseSelectionStore.selectedDepartment}`

        viewTeacherStore.isLoading = true;

        request.get(url,(err,res,body)=>{
            viewTeacherStore.isLoading = false;
            if(err){
                console.log(err);
                alert.showMessage(err.message,"danger");
                return;
            }

            const resp = JSON.parse(body);

            if(resp["status"]==="failure"){
                alert.showMessage(resp["message"],"danger");
                return;
            }
            else{
                viewTeacherStore.getTeacherMisc(viewTeacherStore.tid);
                alert.showMessage("Update succesful","success");
                return;
            }
        })
                                      
    }

})

export default viewTeacherStore;