import React, { useEffect, useState } from "react";
import { Container, Row, Col, Card, CardHeader, CardBody, FormInput,Button } from "shards-react";
import {view} from 'react-easy-state';
import PageTitle from "../../../components/common/PageTitle";
import viewTeacherStore from "./store";
import { AtomSpinner } from "react-epic-spinners";
import PDSChart from './pdsChart';
import AddTeacherLeave from './addTeacherLeave';
import AlertBox from '../../AlertBox';
import teacherLeaveStore from "../../../stores/TeacherLeaveStore";


export default view(()=>{

    return <ViewTeacher 
                isLoading = {viewTeacherStore.isLoading}

                total_subjects={viewTeacherStore.total_subjects}
                teacher_subjects={viewTeacherStore.teacher_subjects}
                
                centralLV = {viewTeacherStore.centralLV}
                centralPeriods={viewTeacherStore.centralPeriods}
                techLV ={viewTeacherStore.techLV}
                techPeriods={viewTeacherStore.techPeriods}
                examLV ={viewTeacherStore.examLV}
                examPeriods ={viewTeacherStore.examPeriods}
                coiLV ={viewTeacherStore.coiLV}
                coiPeriods ={viewTeacherStore.coiPeriods}
                completed_pds = {viewTeacherStore.completed_pds}
                theory_pds ={viewTeacherStore.theory_pds}
                practical_pds = {viewTeacherStore.practical_pds}

                teacher_leave_data = {teacherLeaveStore.teacher_leave_data}
                leaveIsLoading = {teacherLeaveStore.isLoading}

            />
})

const ViewTeacher = ({isLoading,teacher_subjects,
                    total_subjects,centralLV,
                    centralPeriods,techLV,
                    techPeriods,examLV,
                    examPeriods,coiLV,
                    coiPeriods,completed_pds,
                    leaveIsLoading,
                    theory_pds,practical_pds,teacher_leave_data}) => {

    const [hoursPerWeek,setHoursPerWeek] = useState(0);
    const [authorizedLectures,setAuthorizedLectures] = useState(0);
    const [teacherName,setTeacherName] = useState("");
    const [tid,setTeacherId] = useState("");

    var chartData = {
      datasets: [
        {
          hoverBorderColor: "#ffffff",
         data: [theory_pds, practical_pds],
         //data : [10,11],
          backgroundColor: [
            "rgba(0,123,255,0.9)",
            "rgba(0,123,255,0.5)",
          ]
        }
      ],
      labels: ["Theory", "Practical",]
    }

    console.log("Chart data = ",chartData);

    useEffect(()=>{
      const url = window.location.href;
      console.log("Url = ",url);
      const urlParams = new URLSearchParams(window.location.search);
      const tid = urlParams.get("tid");
      console.log("Tid = ",tid);
      setTeacherId(tid);
      teacherLeaveStore.getTeacherLeaves(tid);
      setHoursPerWeek(urlParams.get("weeklyHours"));
      setAuthorizedLectures(urlParams.get("authorized_lectures"));
      setTeacherName(urlParams.get("teacherName"));
      console.log("TID = ",tid);
      viewTeacherStore.getTeacherMisc(tid);
    },[]);
   
    
    return (
      <div>
			<AlertBox />
  <Container fluid className="main-content-container px-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title={teacherName} subtitle="Stats" className="text-sm-left" />
    </Row>

    {/* Default Light Table */}
    <Row>
      <Col> 
        <Stats completed_pds={completed_pds} total_subjects={total_subjects} hoursPerWeek={hoursPerWeek} authorizedLectures={authorizedLectures}/>
        <br />
        <Card small className="mb-4">
          <CardHeader className="border-bottom">
            <h6 className="m-0">MISC</h6>
          </CardHeader>
          <CardBody className="p-0 pb-3">
            {
              (isLoading) &&
              <div>
                <center><AtomSpinner color={"blue"} /></center>
              </div>
            }
            {
              (!isLoading) &&
            <table className="table mb-0">
              <thead className="bg-light">
                <tr>
                  <th scope="col" className="border-0">
                    Assignment
                  </th>
                  <th scope="col" className="border-0">
                    Central Lecture
                  </th>
                  <th scope="col" className="border-0">
                    Technical Seminar
                  </th>
                  <th scope="col" className="border-0">
                    Exam Invigilation
                  </th>
                  <th scope="col" className="border-0">
                    COI
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>LV</td>
                  <td><FormInput type="number" placeholder="Ex : 0 "  value={centralLV} onChange={(evt)=>{if(!evt.target.value){viewTeacherStore.centralLV=0;}  viewTeacherStore.centralLV = parseInt(evt.target.value)}}/></td>
                  <td><FormInput type="number" placeholder="Ex : 0 " value={techLV} onChange={(evt)=>{if(!evt.target.value){viewTeacherStore.techLV = 0;} viewTeacherStore.techLV = parseInt(evt.target.value);}}/></td>
                  <td><FormInput type="number" placeholder="Ex : 0 " value={examLV} onChange={(evt)=>{if(!evt.target.value){viewTeacherStore.examLV=0;}  viewTeacherStore.examLV = parseInt(evt.target.value)}}/></td>
                  <td><FormInput type="number" placeholder="Ex : 0 " value={coiLV} onChange={(evt)=>{if(!evt.target.value){viewTeacherStore.coiLV=0;} viewTeacherStore.coiLV = parseInt(evt.target.value)}}/></td>
                </tr>
                <tr>
                  <td>No Of Periods</td>
                  <td><FormInput type="number" placeholder="Ex : 0 " value={centralPeriods} onChange={(evt)=>{if(!evt.target.value){viewTeacherStore.centralPeriods = 0; } viewTeacherStore.centralPeriods=parseInt(evt.target.value)}}/></td>
                  <td><FormInput type="number" placeholder="Ex : 0 " value={techPeriods}    onChange={(evt)=>{if(!evt.target.value){viewTeacherStore.techPeriods = 0; } viewTeacherStore.techPeriods =parseInt(evt.target.value)}}/></td>
                  <td><FormInput type="number" placeholder="Ex : 0 " value={examPeriods}   onChange={(evt)=>{ if(!evt.target.value){viewTeacherStore.examPeriods = 0; } viewTeacherStore.examPeriods=parseInt(evt.target.value)}}/></td>
                  <td><FormInput type="number" placeholder="Ex : 0 " value={coiPeriods}    onChange={(evt)=>{if(!evt.target.value){viewTeacherStore.coiPeriods = 0; } viewTeacherStore.coiPeriods = parseInt(evt.target.value)}}/></td>
                </tr>
                
                <tr>
                  <td>Total Loading</td>
                  <td>{centralLV*centralPeriods}</td>
                  <td>{techLV * techPeriods}</td>
                  <td>{examLV*examPeriods}</td>
                  <td>{coiLV * coiPeriods}</td>
                </tr>
                <tr>
                  <td><b>Total</b></td>
                  <td>{(centralLV*centralPeriods)+(techLV*techPeriods)+(examLV*examPeriods)+(coiLV*coiPeriods)} </td>
                </tr>
                <tr>
                  <td colSpan={5}><Button theme="primary" onClick={()=>{viewTeacherStore.updateMisc();}}>Update</Button></td>
                </tr>
              </tbody>
            </table>
            }
          </CardBody>
        </Card>
        <Row>
            <Col md={4} lg={4} sm={12} xs={12}>
              <Card style={{padding:10}}>
              
                  <Col md={6} style={{paddingBottom:20}}>
                      Subjects
                  </Col>
                  {
                    teacher_subjects.map((data,index)=>{
                      return <Col><p>{index+1}. {data["subject_name"]}</p></Col>
                    })
                  }
              </Card>
          </Col>
          
          
          <Col md={3} lg={3} sm={12} xs={12}> 
            {
              (completed_pds>0) &&
              <PDSChart title="PDS Chart" chartData={chartData}/>   
            }         
          </Col>
          <Col md={3} lg={4} sm={12} xs={12}>
            <Card>
              <CardHeader>
                <Row>
                  <Col>
                <h6>Teacher Leaves</h6>
                </Col>
                <Col>
                <AddTeacherLeave teacher_id={tid}/>
                </Col>
                </Row>
              </CardHeader>
              <CardBody>
                {
                  (leaveIsLoading) &&
                  <AtomSpinner color={"blue"}/>
                }
                {
                  (!leaveIsLoading) &&
                  teacher_leave_data.map((val,index)=>{
                    return <Row key={index}>
                            <Col md={5} lg={5} sm={5} xs={5}>
                              <h6>{val["start_date"].split("T")[0]}</h6>
                            </Col>
                            <Col md={2} lg={2} sm={2} xs={2}>
                              <h6>to</h6>
                            </Col>
                            <Col md={5} lg={5} sm={5} xs={5}>
                              <h6>{val["end_date"].split("T")[0]}</h6>  
                          </Col> 
                          </Row>
                  })
                }
                 
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          
        </Row>
          
      </Col>
    </Row>
    <br />
    <br />
    <br />

  </Container>
  </div>);
}

const IndividualStats = ({name,stats})=>{
    return (
        <Col md={3} style={{paddingTop:15}}>
          <Card style={{padding:"10px"}}>
              <Row>
                  <Col md={8}>
                      {name}
                  </Col>
                  <Col md={4}>
                      {stats}
                  </Col>
              </Row>
          </Card>
      </Col>
    )
}


const Stats = ({total_subjects,hoursPerWeek,authorizedLectures,completed_pds}) =>{
    return (
    <Col>
        <Row>
            <IndividualStats name={"Total subjects"} stats={total_subjects}/>
            <IndividualStats name={"Hours Per week"} stats={hoursPerWeek} />
            <IndividualStats name={"Authrozied Lectures"} stats={authorizedLectures} />
            <IndividualStats name={"Completed PDS"} stats={completed_pds} />
      </Row>
    </Col>
    )
}
