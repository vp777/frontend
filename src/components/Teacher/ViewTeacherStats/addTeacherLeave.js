import React from "react";
import { Button, Modal, ModalBody, ModalHeader,Col,FormInput, ModalFooter } from "shards-react";
import { AtomSpinner } from "react-epic-spinners";
import {view} from 'react-easy-state';
import addTeacherLeaveStore from '../../../stores/TeacherLeaveStore';
import teacherLeaveStore from "../../../stores/TeacherLeaveStore";

export default view((teacher_id)=>{
    return <BasicModalExample isLoading={addTeacherLeaveStore.isLoading} teacher_id={teacher_id}/>
})

class BasicModalExample extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      open: !this.state.open
    });
  }

  render() {
    const { open } = this.state;
    console.log("Teacher id = ",this.props.teacher_id.teacher_id);
    return (
      <div>
        <Button onClick={this.toggle} theme="primary">Add</Button>
        <Modal open={open} toggle={this.toggle}>
          <ModalHeader>Add Teacher Leave</ModalHeader>
          <ModalBody>
              {
                (!teacherLeaveStore.isLoading) &&
                <Col>
                    <Col>
                        <label>Start Date</label>
                        <FormInput placeholder={"Start Date"} type="date" onChange={(e)=>{addTeacherLeaveStore.start_date = e.target.value}} />
                    </Col>
                    <br />
                    <Col>
                        <label>End DAte</label>
                        <FormInput placeholder={"End Date"} type="date" onChange={(e)=>{addTeacherLeaveStore.end_date = e.target.value}}/>
                    </Col>
              </Col>
            } 
            {
                (teacherLeaveStore.isLoading) &&
                <div style={{padding:"20"}}>
                    <AtomSpinner color={"blue"}/>
                </div>
            }
          </ModalBody>
          <ModalFooter>
              <Button theme="primary" onClick={()=>{addTeacherLeaveStore.addTeacherLeaves(this.props.teacher_id.teacher_id,teacherLeaveStore.start_date,teacherLeaveStore.end_date)}}>Add</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}