import React from 'react';
import TeacherTable from './TeacherTable';
import { Container, Row,Col,Button} from "shards-react";
import teacher_data from '../../stores/TeacherStore';
import PageTitle from "../common/PageTitle";
import AddTeacher from './AddTeacher';
import EditTeacher from './EditTeacher';
import SearchInp from '../common/SearchInp';
import AlertBox from '../AlertBox';
import {view} from 'react-easy-state';
import { Redirect } from "react-router-dom";
import login_data from '../../stores/loginStore';

export default view(()=>{
	var isLoggedIn = login_data.isLoggedIn;
	return (isLoggedIn?<Teacher />:<Redirect to="/login" />)
	//return <Teacher />
})

class Teacher extends React.Component{
	constructor(props){
		super(props);
		this.state = {addteacher:false,editteacher:true};

		this.addTeacher = this.addTeacher.bind(this);
		this.removeTeacher = this.removeTeacher.bind(this);
		this.closeEditTeacher = this.closeEditTeacher.bind(this);
		this.EditTeacher = this.EditTeacher.bind(this);
	}

	addTeacher(){
		this.setState({addteacher:true});
	}

	removeTeacher(){
		this.setState({addteacher:false});
	}

	closeEditTeacher(){
		this.setState({editteacher:false});
	}
	EditTeacher(){
		this.setState({editteacher:true});
	}

	render(){
		return(
			<div>
			<AlertBox />
			<Container fluid className="main-content-container px-4">
      		{/* Page Header */}
      		<Row noGutters className="page-header py-4">
        			<PageTitle sm="4" title="Teacher" subtitle="Manage" className="text-sm-left" />
      		</Row>
      			<div className="pt-3"><Button theme="primary" size="sm" onClick={this.addTeacher}>Add New Lecturer</Button></div>
			  	<Row>
			  			<Col md="4" sm="4">
			  				<div className="pt-3 mr-2">
			  					<SearchInp fields={[{value:'name',name:'Name'},{value:'teacher_id',name:'TeacherID'},{value:'phone',name:'Phone'},{value:'email',name:'Email'}]}
											  setAttribute={(evt)=>{teacher_data.search_attribute=evt.target.value}}
											  search={(evt)=>{teacher_data.search(evt.target.value)}}
								  			/>
			  				</div>
			  			</Col>	
			   </Row>
			   		<AddTeacher close={this.removeTeacher} open={this.state.addteacher} />
					<EditTeacher close={this.closeEditTeacher} open={this.state.editteacher} />
					<TeacherTable />
			</Container>
			</div>
		)
	}
}