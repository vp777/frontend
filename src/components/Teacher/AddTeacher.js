import React from "react";
import {  Modal, ModalBody, ModalHeader,Col,Button} from "shards-react";
import {view} from 'react-easy-state';
import teacher_data from '../../stores/TeacherStore';
import Input from '../common/Input';

export default view((props)=>{
  var isLoading = teacher_data.isLoading;

  return <AddTeacher isLoading={isLoading} open={props.open} close={()=>{props.close()}}/>
});


function setName(name){
    teacher_data.name = name; 
}

function setPhone(phone){
    teacher_data.phone = phone;
}

function setEmail(email){
    teacher_data.email = email;
}

function setAuthorizedPeriods(n){
  teacher_data.authorized_periods = n;
  console.log("Authorized periods = ",n);
}

function addTeacher(){
  teacher_data.addTeacher();
}

class AddTeacher extends React.Component{
       constructor(props) {
            super(props);
            this.state = { open: false };
            this.toggle = this.toggle.bind(this);
        }

        componentDidMount(){
            this.setState({open:this.props.open});
        }

        componentDidUpdate(prevProps){
            if(prevProps.open!==this.props.open){
                this.setState({open:this.props.open});
            }
        }
    
      toggle() {
        this.setState({
          open: !this.state.open
        });

        this.props.close();
      }
    
      render() {
        const { open } = this.state;
        return (
          <div>
            <Modal open={open} toggle={this.toggle}>
              <ModalHeader>
                    <b>Add Teacher</b>     
            </ModalHeader>
              <ModalBody>
                <Input size="8" name="Name" type="text" onChange={(evt)=>{setName(evt.target.value)}} />
                <Input size="8" name="Phone" type="text" onChange={(evt)=>{setPhone(evt.target.value)}} />
                <Input size="8" name="Email" type="text" onChange={(evt)=>{setEmail(evt.target.value)}} />
                <Input size="8" name="Authorized Periods" type="number" onChange={(evt)=>{setAuthorizedPeriods(evt.target.value)}} />
            
               
                <Col size="8" className={"pt-3"}>
                    <Button theme="primary" onClick={()=>{addTeacher();this.toggle();}}>Add Teacher</Button>
                </Col>
              </ModalBody>
            </Modal>
          </div>
        );
      }
}