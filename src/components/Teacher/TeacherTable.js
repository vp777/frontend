import React from "react";
import {  Row, Col, Card, CardHeader, CardBody,Button } from "shards-react";
import EditButton from '../common/EditButton';
import DeleteButton from '../common/DeleteButton';
import {view} from 'react-easy-state';
import Message from '../Message';
import {Link} from 'react-router-dom';
import { AtomSpinner } from 'react-epic-spinners';
import teacher_data from '../../stores/TeacherStore';
import '../scroll.css';

export default view(()=>{
    console.log(teacher_data.viewData);
    return <TeacherTable isLoading={teacher_data.isLoading}
                        data={teacher_data.viewData}
                        hasError = {teacher_data.hasFetchError}
                        />
})


class TeacherTable extends React.Component{
  componentDidMount(){
    teacher_data.getData();
  }

  render(){
    return(
     
      
      <Row>
      <Col>
        <Card small className="mb-4">
          <CardHeader className="border-bottom">
            <h6 className="m-0">Teachers</h6>
          </CardHeader>
          <CardBody className="p-0 pb-3 scroll_list-group">
          { (!this.props.isLoading && (this.props.data.length>0) && (!this.props.hasError)) &&
            <table className="table mb-0 ">
              <thead className="bg-light">
                <tr>
                  <th scope="col" className="border-0">
                    TeacherID
                  </th>
                  <th scope="col" className="border-0">
                    Name
                  </th>
                  <th scope="col" className="border-0">
                    Phone
                  </th>
                  <th scope="col" className="border-0">
                    Email
                  </th>
                  <th scope="col" className="border-0">
                    Teaching Hours/Week
                  </th>
                  <th scope="col" className="border-0">
                    Authorized Periods
                  </th>
                  <th scope="col" className="border-0">
                      View
                  </th>
                  <th scope="col" className="border-0">
                      Edit
                  </th>
                  <th scope="col" className="border-0">
                      Remove
                  </th>
                </tr>
              </thead>
              <tbody>
               
                {
                      this.props.data.map((item,i)=>{
                                return (<Trow key={i} data={item} index={i+1}/>)
                      })
                  }

                
                
              </tbody>
            </table>
          }

            {
              this.props.isLoading && 
              <center>
                  <AtomSpinner color={'blue'} />
              </center>
            }
            {
                ((this.props.data.length===0) && !this.props.isLoading && (!this.props.hasError)) &&
                <Message message={"Kindly Add a Teacher"} icon_name={'call_missed'} />
            }
            {
              (!this.props.isLoading && (this.props.hasError)) &&
              <Message message={"Sorry , We failed to fetch data"}  icon_name={'warning'} />
            }
          </CardBody>
        </Card>
      </Col>
    </Row>
  
    )
  }
}



class Trow extends React.Component{
  render(){
    return(
      <tr>
          <td>{this.props.data.tid}</td>
          <td>{this.props.data.name}</td>
          <td>{this.props.data.phone}</td>
          <td>{this.props.data.email}</td>
          <td>{this.props.data.no_of_hours_per_week}</td>
          <td>{this.props.data.authorized_lectures}</td>
          <td><Link to={"/viewteacher?tid="+this.props.data.tid+"&weeklyHours="+this.props.data.no_of_hours_per_week+"&authorized_lectures="+this.props.data.authorized_lectures+"&teacherName="+this.props.data.name}><Button>View</Button></Link></td>
          <td><EditButton onClick={()=>{teacher_data.setEditTeacher(this.props.data)}}/></td>
          <td><DeleteButton onClick={()=>{teacher_data.removeTeacher(this.props.data.tid)}}/></td>
    </tr>
      )
  }
}
