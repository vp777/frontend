import React from "react";
import {
  Form,
} from "shards-react";
import databaseSelectionStore from "../../databaseSelectionStore";
import {view} from 'react-easy-state';


export default view(() => (
  <Form className="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
     {/* <img
                id="main-logo"
                className="d-inline-block align-top ml-4"
                style={{ maxWidth: "60px" }}
                src={require("../../../images/bitlogo.png")}
                alt="Shards Dashboard"
     />
     */}
      <h6 className="m-3">{databaseSelectionStore.selectedDepartment}</h6>
     
      </Form>
));
