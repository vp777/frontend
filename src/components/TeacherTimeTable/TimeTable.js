import React from 'react';
import {Col,Card,CardHeader,CardBody,Row,Button} from 'shards-react';
import {AtomSpinner} from 'react-epic-spinners';
import SelectTeacher from '../SelectTeacher';
import {view} from 'react-easy-state';
import Message from '../Message';
import tt_data from '../../stores/TeacherTimeTable';

export default view(()=>{
    console.log(tt_data.day_data);
    return <TimeTableView  isLoading={tt_data.isLoading} data={tt_data.day_data} teacher_id={tt_data.teacher_id}/>;
})

class TimeTableView extends React.Component{

    render(){
        console.log(this.props.data);
        return(
            <Col>
                <Card small className="mb-4"  >
                    <CardHeader className="border-bottom">
                            <h6 className="m-0">Teacher TimeTable</h6>
                    </CardHeader>
        
                    <CardBody className="pt-0 scroll_list-group">
                        
                        <Row className="border-bottom py-2 bg-light">
                            <Col sm="4" className="d-flex mb-2 mb-sm-0">
                                    <SelectTeacher onChange={(evt)=>{tt_data.setTeacherId(evt.target.value)}}/>
                            </Col>
                        </Row>

                    {

                        ((!this.props.isLoading) && (this.props.data.length>0)  ) &&
                        
                        <table className="table mb-0 ">
                            <thead className="bg-light">
                            
                            <tr>
                                <th scope="col" className="border-0">
                                    #
                                </th>
                                <th scope="col" className="border-0">
                                    Subject
                                </th>
                                <th scope="col" className="border-0">
                                    Class
                                </th>
                                <th scope="col" className="border-0">
                                    Room No.
                                </th>
                                <th scope="col" className="border-0">
                                    Start Time
                                </th>
                                <th scope="col" className="border-0">
                                    End Time
                                </th>
                                
                            </tr>

                            </thead>
                            
                            <tbody>
                            {
                                this.props.data.map((item,i)=>{
                                    return (<Trow key={i} data={item} index={i+1}/>)
                                })
                            }
               
                            </tbody>
                        </table>

                    }
                    {
                        (this.props.isLoading) &&
                        <center>
                                 <AtomSpinner color={'blue'} />
                        </center>
                    }
                    {
                        ((this.props.teacher_id===undefined) && (!this.props.isLoading)) &&
                        <Message icon_name={"call_missed"} message={"Please Select a Lecturer"} />
                    }
                    {
                        ((this.props.data.length===0) && (this.props.teacher_id!==undefined) && (!this.props.isLoading) ) &&
                        <Message icon_name={"airline_seat_individual_suite"} message={"You Have No Class Today "} />
                    }
                 </CardBody>
             </Card>
            </Col>
        
        )
    }
}


function Trow(props){
    return(
         <tr>
                <td>{props.index}</td>
                <td>{props.data.subject_name}</td>
                <td>{props.data.class_name}</td>
                <td>{props.data.room_id}</td>
                <td>{props.data.start_time}</td>
                <td>{props.data.end_time}</td>
        </tr>
    )
}

