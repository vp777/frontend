import React from 'react';
import { Container, Row} from "shards-react";
import PageTitle from "../common/PageTitle";
import Days from './Days';
import TimeTableView from './TimeTable';

import {view} from 'react-easy-state';
import { Redirect } from "react-router-dom";
import login_data from '../../stores/loginStore';

export default view(()=>{
	var isLoggedIn = login_data.isLoggedIn;
	//return (isLoggedIn?<TimeTable />:<Redirect to="/login" />)
	return <TimeTable />
})

 class TimeTable extends React.Component{
	
	render(){
		return(
			<Container fluid className="main-content-container px-4">
      		{/* Page Header */}
      		<Row noGutters className="page-header py-4">
        			<PageTitle sm="4" title="Teacher TimeTable" subtitle="View" className="text-sm-left" />
      		</Row>
      		<Days />

            <TimeTableView />
					
			</Container>
		)
	}
}
