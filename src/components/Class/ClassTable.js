import React from "react";
import {  Row, Col, Card, CardHeader, CardBody ,Button} from "shards-react";
import ViewButton from '../common/ViewButton';
import EditButton from '../common/EditButton';
import DeleteButton from '../common/DeleteButton';
import Message from '../Message';
import class_data from '../../stores/classStore';
import { AtomSpinner } from 'react-epic-spinners';
import {view} from 'react-easy-state';

export default view(()=>{

      return <ClassTable data={class_data.viewData} isLoading={class_data.isLoading} />
})


function removeClass(id){
  class_data.removeClassData(id);
}

function modifyClass(id,name){
  class_data.modifyClass(id,name);
}

class ClassTable extends React.Component{
  
  componentDidMount(){
      class_data.getClassData();
  }

  render(){
    return(
     
      
      <Row>
      <Col>
        <Card small className="mb-4">
          <CardHeader className="border-bottom">
            <h6 className="m-0">Class</h6>
          </CardHeader>
          <CardBody className="p-0 pb-3 scroll_list-group">
          {
           ( (!this.props.isLoading) && this.props.data.length>0) && 
            <table className="table mb-0 ">
              <thead className="bg-light">
                <tr>
                  <th scope="col" className="border-0">
                    #
                  </th>
                  <th scope="col" className="border-0">
                    Class Name
                  </th>
                  <th scope="col" className="border-0">
                    No. of Subjects
                  </th>
                  <th scope="col" className="border-0">
                    No. of Hours/Week
                  </th>
                  <th scope="col" className="border-0">
                    Class Strength
                  </th>
                  <th scope="col" className="border-0">
                      Edit
                  </th>
                  <th scope="col" className="border-0">
                      Remove
                  </th>
                </tr>
              </thead>
              <tbody>
                  {
                      this.props.data.map((item,i)=>{
                                return (<Trow key={i} data={item} index={i+1}/>)
                      })
                  }
                
              </tbody>
            </table>
          }
          {
            this.props.isLoading &&
            <center>
              <AtomSpinner color={'blue'} />
            </center>
          }
          {
            ((this.props.data.length===0) && !this.props.isLoading) &&
            <Message message={"Kindly Add a Class"} icon_name={'call_missed'} />
          }
          </CardBody>
        </Card>
      </Col>
    </Row>
  
    )
  }
}




class Trow extends React.Component{
  render(){
    return(
                <tr>
                  <td>{this.props.data.class_id}</td>
                  <td>{this.props.data.class_name}</td>
                  <td>{this.props.data.no_of_subjects}</td>
                  <td>{this.props.data.no_of_hours_per_week}</td>
                  <td>{this.props.data.class_strength}</td>
                  <td onClick={()=>modifyClass(this.props.data.class_id,this.props.data.class_name)}><EditButton/></td>
                  <td onClick={()=>removeClass(this.props.data.class_id)}><DeleteButton /></td>
                </tr>
      )
  }
}