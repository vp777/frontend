import React from "react";
import {  Modal, ModalBody, ModalHeader,Col,Button} from "shards-react";
import { AtomSpinner } from 'react-epic-spinners';
import Input from '../common/Input';
import {view} from 'react-easy-state';
import class_data from '../../stores/classStore';


function setClassName(classname){
  class_data.setClassName(classname);
}

function setClassStrength(n){
  class_data.setClassStrength(n);
}

function addClassName(className){
  class_data.addClass(className);
}

export default view((props)=>{
    var isLoading = class_data.isLoading;
    console.log(isLoading);
    return <AddClass isLoading={isLoading} open={props.open} close={()=>props.close()}/>;
})

class AddClass extends React.Component{
       constructor(props) {
            super(props);
            this.state = { open: false };
            this.toggle = this.toggle.bind(this);
        }

        componentDidMount(){
            this.setState({open:this.props.open});
        }

        componentDidUpdate(prevProps){
            if(prevProps.open!==this.props.open){
                this.setState({open:this.props.open});
            }
        }
    
      toggle() {
        console.log("Toggling from "+this.state.open+" to "+(!this.state.open));
        this.setState({
          open: !this.state.open
        });
        
        this.props.close();
      }
    
      render() {
        const { open } = this.state;
        return (
          <div>

            <Modal open={open} toggle={this.toggle}>
              <ModalHeader>
                    <b>Add Class</b>     
            </ModalHeader>
              <ModalBody>
              {
                !this.props.isLoading &&
                <div>
                    <Input size="8" name="Class Name" type="text" onChange={(evt)=>{setClassName(evt.target.value)}} />
                    <Input size="8" name="Class Strength" type="number" onChange={(evt)=>{setClassStrength(evt.target.value)}} />
                    <Col size="8" className={"pt-3"}><Button theme="primary" onClick={()=>{addClassName();this.toggle();}}>Add Class</Button></Col>
                </div>
              }
              
              {
                this.props.isLoading &&
                <center>
                  <AtomSpinner color="blue"></AtomSpinner>
                </center>
              }
              </ModalBody>
            </Modal>
          </div>
        );
      }
}