import React from 'react';
import ClassTable from './ClassTable';
import { Container, Row,Col,Button,Alert} from "shards-react";
import PageTitle from "../common/PageTitle";
import AddClass from './AddClass';
import SearchInp from '../common/SearchInp';
import AlertBox from '../AlertBox';
import class_data from '../../stores/classStore';
import {view} from 'react-easy-state';
import { Redirect } from "react-router-dom";
import login_data from '../../stores/loginStore';

export default view(()=>{
	var isLoggedIn = login_data.isLoggedIn;
	return (isLoggedIn?<Class />:<Redirect to="/login" />)
	//return <Class />;
})

class Class extends React.Component{
	constructor(props){
		super(props);
		this.state = {addClass:false};

		this.addClass = this.addClass.bind(this);
		this.removeClass = this.removeClass.bind(this);
	}

	addClass(){
		console.log("Add class = ",this.state.addClass);
		this.setState({addClass:false},
						()=>{
							this.setState({addClass:true})
						}
		);
	}

	
	removeClass(){
		console.log("Add Class = ",this.state.addClass);
		this.setState({addClass:false});
	}

	render(){
		return(
			<div>
			<AlertBox />
			<Container fluid className="main-content-container px-4">
      		{/* Page Header */}
      		
      		<Row noGutters className="page-header py-4">
        			<PageTitle sm="4" title="Class" subtitle="Manage" className="text-sm-left" />
      		</Row>
      			<div className="pt-3"><Button theme="primary" size="sm" onClick={this.addClass}>Add New Class</Button></div>
			  	<Row>
			  			<Col md="4" sm="4">
							<div className="pt-3 mr-2">
							  	<SearchInp search={(evt)=>class_data.search(evt.target.value)} fields={[{value:'class_name',name:"Class Name"}]}/>
							</div>
						</Col>	
			   </Row>
			   		<AddClass close={this.removeClass} open={this.state.addClass} />
					<ClassTable />
			</Container>
			</div>
		)
	}
}