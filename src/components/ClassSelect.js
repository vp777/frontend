import React from "react";
import { 
  		FormSelect ,
	} from "shards-react";
import { view } from 'react-easy-state';
import class_data from '../stores/classStore';



export default view((props)=>{     
                                  return <SelectClass class_data={class_data.data} 
                                                   	  isLoading={class_data.isLoading}
                                                   	  onChange = {props.onChange}
                                          />
                      });

class SelectClass extends React.Component{

	componentDidMount(){
            console.log("Running ClassNames");
            class_data.getClassData();
	}


  render(){

     

    return (
    
                
     <FormSelect onChange ={this.props.onChange}>
         
        <option value={undefined}>{this.props.isLoading?"Loading...":"Select Class"}</option>
        {

               this.props.class_data.map((item,i)=>{
                         return (<option key={i} value={item.class_id}>{item.class_name}</option>)
                  })
              
        } 
      
      </FormSelect>
     
  )
  }
}

