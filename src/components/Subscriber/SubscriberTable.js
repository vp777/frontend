import React from "react";
import {  Row, Col, Card, CardHeader, CardBody ,Button} from "shards-react";
import ViewButton from '../common/ViewButton';
import EditButton from '../common/EditButton';
import DeleteButton from '../common/DeleteButton';



export default class SubscriberTable extends React.Component{
  render(){
    return(
     
      
      <Row>
      <Col>
        <Card small className="mb-4">
          <CardHeader className="border-bottom">
            <h6 className="m-0">Subscribers</h6>
          </CardHeader>
          <CardBody className="p-0 pb-3 scroll_list-group">
            <table className="table mb-0 ">
              <thead className="bg-light">
                <tr>
                  <th scope="col" className="border-0">
                    #
                  </th>
                  <th scope="col" className="border-0">
                    SubscriberID
                  </th>
                  <th scope="col" className="border-0">
                    Name
                  </th>
                  <th scope="col" className="border-0">
                    Email
                  </th>
                  <th scope="col" className="border-0">
                    Phone
                  </th>
                  <th scope="col" className="border-0">
                    Subscribed Classes
                  </th>
                  <th scope="col" className="border-0">
                      Add Class
                  </th>                 
                  <th scope="col" className="border-0">
                      Edit
                  </th>
                  <th scope="col" className="border-0">
                      Remove
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>15CSLABC</td>
                  <td>ABC</td>
                  <td>BITIST001</td>
                  <td>4</td>
                  <td>1hr</td>
                  <td><Button theme="primary">Add</Button></td>
                  <td><EditButton/></td>
                  <td><DeleteButton /></td>
                </tr>
                
              </tbody>
            </table>
          </CardBody>
        </Card>
      </Col>
    </Row>
  
    )
  }
}



