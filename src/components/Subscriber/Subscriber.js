import React from 'react';
import SubscriberTable from './SubscriberTable';
import { Container, Row,Col,Button,FormSelect,InputGroupText,InputGroup,InputGroupAddon} from "shards-react";
import PageTitle from "../common/PageTitle";
import AddSubscriber from './AddSubscriber';
import SearchInp from '../common/SearchInp';



export default class Subscriber extends React.Component{
	constructor(props){
		super(props);
		this.state = {addSubscriber:false};

		this.addSubscriber = this.addSubscriber.bind(this);
		this.removeSubscriber = this.removeSubscriber.bind(this);
	}

	addSubscriber(){
		this.setState({addSubscriber:true});
	}

	removeSubscriber(){
		this.setState({addSubscriber:false});
	}

	render(){
		return(
			<Container fluid className="main-content-container px-4">
      		{/* Page Header */}
      		<Row noGutters className="page-header py-4">
        			<PageTitle sm="4" title="Subscriber" subtitle="Manage" className="text-sm-left" />
      		</Row>
      			<div className="pt-3"><Button theme="primary" size="sm" onClick={this.addSubscriber}>Add New Subscriber</Button></div>
			  	<Row>
					  	<Col md="4" sm="4" className="pt-3 mr-2"><SelectClass /></Col>
			  			<Col md="4" sm="4"><div className="pt-3 mr-2"><SearchInp fields={[{value:'sub_name',name:"Subscriber Name"},{value:"email",name:'Email'},{value:"phone",name:'Phone'}]}/></div></Col>	
			   </Row>
			   		<AddSubscriber close={this.removeSubscriber} open={this.state.addSubscriber} />
					<SubscriberTable />
			</Container>
		)
	}
}


class SelectClass extends React.Component{
	render(){
	  return(

		<InputGroup className="mb-3">
		<FormSelect>
					<option>Class 1</option>
					<option>Class 2</option>
					<option>Class 3</option>
					<option>Class 4</option>
		</FormSelect>
		<InputGroupAddon type="append">
			<InputGroupText>Select Class</InputGroupText>
		</InputGroupAddon>
  </InputGroup>
	
		
	 
	  )
	}
  }