import React from "react";
import {  Modal, ModalBody, ModalHeader,Col,Button,FormSelect,Row } from "shards-react";
import Input from '../common/Input';

export default class AddSubscriber extends React.Component{
       constructor(props) {
            super(props);
            this.state = { open: false };
            this.toggle = this.toggle.bind(this);
        }

        componentDidMount(){
            this.setState({open:this.props.open});
        }

        componentDidUpdate(prevProps){
            if(prevProps.open!==this.props.open){
                this.setState({open:this.props.open});
            }
        }
    
      toggle() {
        this.setState({
          open: !this.state.open
        });

        this.props.close();
      }
    
      render() {
        const { open } = this.state;
        return (
          <div>
            <Modal open={open} toggle={this.toggle}>
              <ModalHeader>
                    <b>Add Subscriber</b>     
            </ModalHeader>
              <ModalBody>
                    <Input size="8" name="Name" type="text" onChange={(evt)=>{console.log(evt.target.value)}} />
                    <Input size="8" name="Email ID" type="text" onChange={(evt)=>{console.log(evt.target.value)}} />
                    <Input size="8" name="Phone" type="text" onChange={(evt)=>{console.log(evt.target.value)}} />
                    <SelectClass />
                <Col size="8" className={"pt-3"}><Button theme="primary">Add Subscriber</Button></Col>
              </ModalBody>
            </Modal>
          </div>
        );
      }
}

class SelectClass extends React.Component{
  render(){
    return(
    <Col md={"6"} className="form-group">
      <label >{"Class"}</label>
      <FormSelect>
                  <option>Class 1</option>
                  <option>Class 2</option>
                  <option>Class 3</option>
                  <option>Class 4</option>
      </FormSelect>
    </Col>
    )
  }
}

class SelectTeacher extends React.Component{
  render(){
    return(
    <Col md={"6"} className="form-group">
      <label >{"Teacher"}</label>
      <FormSelect>
                  <option>None</option>
                  <option>TID1(Venkatesh)</option>
                  <option>TID2(ABCDEf)</option>
                  <option>TID3(ASDFA)</option>
                  <option>TID4(ASDSDS)</option>
      </FormSelect>
    </Col>
    )
  }
}