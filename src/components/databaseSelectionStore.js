import {store} from 'react-easy-state';

const databaseSelectionStore = store({

    departmentNames : [ 
        "Faculty of Degree Engineering",
        "Faculty of Aeronautics",
        "Faculty of Electronics",
        "Cadets Training Wing",
        "Faculty of Equipment Management and Tactics",
        "Faculty of Electrical and Mechanical Engineering",
    ],

    selectedDepartment : null,

})

export default databaseSelectionStore;