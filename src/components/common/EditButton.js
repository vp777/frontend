import React from "react";
import {  Button } from "shards-react";
export default class EditButton extends React.Component{
    render(){
      return(
  
        <div>
          <Button outline onClick={this.props.onClick} theme="success" className="mb-2 mr-1">
                <i class="material-icons">edit  </i>
            </Button>
        </div>
  
      )
    }
  }