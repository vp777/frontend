import React from "react";
import {  Button } from "shards-react";
export default class DeleteButton extends React.Component{
    render(){
      return(
  
        <div>
          <Button outline theme="danger" className="mb-2 mr-1" onClick={this.props.onClick}>
                <i class="material-icons">delete_forever</i>
            </Button>
        </div>
  
      )
    }
  }