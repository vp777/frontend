import React from 'react';
import {InputGroup,FormInput,InputGroupAddon,FormSelect} from 'shards-react';

export default  class SearchInp extends React.Component{
	render(){
		return(
			<InputGroup className="mb-3">
      			<FormInput  placeholder="Search ..." onChange={this.props.search} />
      			<InputGroupAddon type="append">
        			 <FormSelect onChange={this.props.setAttribute}>
                  {
                          this.props.fields.map((item,i)=>{
                                return (<option key={i} value={item.value}>{item.name}</option>)
                          })
                  
                  }
        			</FormSelect>
      			</InputGroupAddon>
    		</InputGroup>
		)
	}
}