import React from "react";
import {Col} from "shards-react";


const FileInput = (props) => (
    <Col md={props.size} className="form-group">
         <label >{props.name}</label>
         <div  className="custom-file mb-0">
                <input type="file" className="custom-file-input" id="customFile2" />
                <label className="custom-file-label" htmlFor="customFile2">
                    Choose file...
                </label>
        </div>
    </Col>
);


export default FileInput;
