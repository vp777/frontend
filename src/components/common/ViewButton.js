import React from "react";
import {  Button } from "shards-react";
export default class ViewBtn extends React.Component{
    render(){
      return(
  
        <div>
          <Button theme="primary" onClick={this.props.onClick}>View</Button>
        </div>
  
      )
    }
  }