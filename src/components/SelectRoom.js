import React from "react";
import { 
  		FormSelect ,
	} from "shards-react";
import { view } from 'react-easy-state';
import room_data from '../stores/roomStore';



export default view((props)=>{     
                                  return <SelectRoom data={room_data.data} 
                                                   	  isLoading={room_data.isLoading}
                                                   	  onChange = {props.onChange}
                                          />
                      });

class SelectRoom extends React.Component{
	componentDidMount(){    
            console.log("Running Teacher");
            room_data.getRoomData();
	}


  render(){
    return (     
     <FormSelect onChange ={this.props.onChange}>
         
        <option value={null}>{this.props.isLoading?"Loading...":"Select Room"}</option>
        {

               this.props.data.map((item,i)=>{
                         return (<option key={i} value={item.room_no}>{item.room_no+" ("+item.room_name+")"}</option>)
                })
              
        } 
      </FormSelect>
  )
  }
}

