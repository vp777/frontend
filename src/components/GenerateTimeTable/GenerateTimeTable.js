import React from 'react';
import { Container, Row,Col,Button,FormInput,Card,CardHeader,CardBody} from "shards-react";
import PageTitle from "../common/PageTitle";
import AlertBox from '../AlertBox';
import {view} from 'react-easy-state';
import gen_time_data from '../../stores/GenTimeTable';
import {AtomSpinner} from 'react-epic-spinners';
import { Redirect } from "react-router-dom";
import login_data from '../../stores/loginStore';


export default view(()=>{
    var isLoggedIn = login_data.isLoggedIn;
     return (isLoggedIn?<GenTimeTable isLoading={gen_time_data.isLoading} />:<Redirect to="/login" />)
    //return <GenTimeTable isLoading={gen_time_data.isLoading} />
})

class GenTimeTable extends React.Component{
    constructor(props){
        super(props);
        this.state = {
                    no_of_breaks : 0,
                    breaks:[],
                    break_timings:[],

                    day_timings : {
                        monday :{
                            start_time : undefined,
                            end_time   : undefined,
                        },
                        tuesday : {
                            start_time : undefined,
                            end_time   : undefined,
                        },
                        wednesday : {
                            start_time : undefined,
                            end_time   : undefined,
                        },
                        thursday : {
                            start_time : undefined,
                            end_time   : undefined,
                        },
                        friday : {
                            start_time : undefined,
                            end_time   : undefined
                        },
                        saturday : {
                            start_time : undefined,
                            end_time   : undefined
                        }
                    }
                };

        this.onUpdateNoOfBreaks     = this.onUpdateNoOfBreaks.bind(this);
        this.updateBreakEndTime     = this.updateBreakEndTime.bind(this);
        this.updateBreakStartTime   = this.updateBreakStartTime.bind(this);
        this.updateDayStartTime     = this.updateDayStartTime.bind(this);
        this.updateDayEndTime       = this.updateDayEndTime.bind(this);
        this.formatData             = this.formatData.bind(this);
        this.setMondayTimeForAll    = this.setMondayTimeForAll.bind(this);
    }

    

    updateBreakStartTime(time,idx){
        var timings = this.state.break_timings;
        timings[idx].start_time = time;
        console.log(timings);
        this.setState({break_timings:timings});
    }

    updateBreakEndTime(time,idx){
        var timings = this.state.break_timings;
        timings[idx].end_time = time;
        this.setState({break_timings:timings});
    }
    
    onUpdateNoOfBreaks(){
        var n = this.state.no_of_breaks;
        var arr = [];
        var time_arr = [];
        for(var i=0;i<n;i++){
            arr.push(i);
            time_arr.push({
                start_time : undefined,
                end_time : undefined
            });
        }
        this.setState({breaks:arr,break_timings:time_arr});
    }

    updateDayStartTime(day,time){
        var timings = this.state.day_timings;
        timings[day].start_time = time;
        this.setState({day_timings:timings});
        console.log(this.state.day_timings);
    }

    updateDayEndTime(day,time){
        var timings = this.state.day_timings;
        timings[day].end_time = time;
        this.setState({day_timings:timings});
        console.log(this.state.day_timings);
    }

    setMondayTimeForAll(){
        var mondaytime = this.state.day_timings["monday"];

        var daytimings = this.state.day_timings;

        Object.keys(daytimings).forEach((day)=>{
            daytimings[day] = mondaytime;
        })


        this.setState({day_timings:daytimings});

    }



    formatData(){
        return JSON.stringify(this.state);
    }

    render(){
        var that = this;
		return(
            <div>
				<AlertBox />
                    <Container fluid className="main-content-container px-4">
      		            {/* Page Header */}
      		            <Row noGutters className="page-header py-4">
        			        <PageTitle sm="4" title="TimeTable" subtitle="Generate" className="text-sm-left" />
      		            </Row>
                {
                    (!this.props.isLoading) &&
                    <div>
                        <div>
                            <Row>
                            <Col md="3" lg="3">
                                <label>No of Breaks</label>
                                <FormInput placeholder="No of Breaks" onChange={(evt)=>{this.setState({no_of_breaks:evt.target.value},this.onUpdateNoOfBreaks)}} />
                            </Col>
                            </Row>
                        </div>
                        <div className="mt-4">
                            
                            <Col lg="12">
                            <Row>
                                    {
                    
                                        this.state.breaks.map(function(i){
                                           return <BreakInp index={i+1} key={i} setStartTime={(val)=>{that.updateBreakStartTime(val,i)}} setEndTime={(val)=>{that.updateBreakEndTime(val,i)}} /> 
                                        })
                                    }
    
                            </Row>
                            </Col>
                        </div>
                        <div className="mt-4">
                        <Col lg="12" >
                        {
                            (this.state.day_timings["monday"].start_time!==undefined) &&
                            <Button onClick={()=>{this.setMondayTimeForAll();}}>Duplicate Monday</Button>
                        }
                        <Row>
                            <DayTime day="Monday" setStartTime={(value)=>{this.updateDayStartTime('monday',value)}}  setEndTime={(value)=>{this.updateDayEndTime('monday',value)}} />
                            <DayTime day="Tuesday" setStartTime={(value)=>{this.updateDayStartTime('tuesday',value)}} setEndTime={(value)=>{this.updateDayEndTime('tuesday',value)}} />
                            <DayTime day="Wednesday" setStartTime={(value)=>{this.updateDayStartTime('wednesday',value)}} setEndTime={(value)=>{this.updateDayEndTime('wednesday',value)}} /> 
                            <DayTime day="Thursday" setStartTime={(value)=>{this.updateDayStartTime('thursday',value)}} setEndTime={(value)=>{this.updateDayEndTime('thursday',value)}} />
                            <DayTime day="Friday" setStartTime={(value)=>{this.updateDayStartTime('friday',value)}} setEndTime={(value)=>{this.updateDayEndTime('friday',value)}} />
                            <DayTime day="Saturday" setStartTime={(value)=>{this.updateDayStartTime('saturday',value)}} setEndTime={(value)=>{this.updateDayEndTime('saturday',value)}} />
            
                        </Row>  
                        <Button theme="primary" className="mt-4 mb-4" onClick={()=>{gen_time_data.genTimeTable(this.formatData())}}>Generate Time Table</Button>
                        </Col>
                        </div>
                        </div>
                    }
                    {
                        (this.props.isLoading) &&
                        <center>
                                <AtomSpinner color={'blue'} />
                        </center>
                    }

                    </Container>
			</div>
        )
    }

}

function BreakInp(props){
    return(
        <Col lg="4" md="4">
            <Card small className="mb-4">
                <CardHeader className="border-bottom">
                            <h6>{"Break "+props.index}</h6>
                </CardHeader>
                <CardBody>
                            <Row>
                                    <Col lg="6">
                                        <label>Start Time</label>
                                        <FormInput placeholder="Start Time"  type="time" onChange={(evt)=>{props.setStartTime(evt.target.value)}}/>
                                    </Col>
                                    <Col lg="6">
                                        <label>End Time</label>
                                        <FormInput placeholder="End Time"  type="time" onChange={(evt)=>{props.setEndTime(evt.target.value)}}/>
                                    </Col>
                            </Row>
                </CardBody>

            </Card>
        </Col>
    )
}

function DayTime(props){
    return(
    <Col lg="4" md="4">
        <Card small className="mb-4">
            <CardHeader className="border-bottom">
                <h6>{props.day}</h6>
            </CardHeader>
            <CardBody>
                <Row>
                    <Col lg="6">
                        <label>Start Time</label>
                        <FormInput placeholder="Start Time"  type="time" onChange={(evt)=>{props.setStartTime(evt.target.value)}}/>
                    </Col>
                    <Col lg="6">
                        <label>End Time</label>
                        <FormInput placeholder="End Time"  type="time" onChange={(evt)=>{props.setEndTime(evt.target.value)}}/>
                    </Col>
                </Row>
             </CardBody>
       
        </Card>
    </Col>
    )
}