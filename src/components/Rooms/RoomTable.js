import React from "react";
import {  Row, Col, Card, CardHeader, CardBody } from "shards-react";
import DeleteButton from '../common/DeleteButton';
import Message from '../Message';
import { AtomSpinner } from 'react-epic-spinners';
import {view} from 'react-easy-state';
import room_data from '../../stores/roomStore';

export default view(()=>{

	return <RoomTable isLoading={room_data.isLoading} data={room_data.viewData}/>
})


class RoomTable extends React.Component{

  componentDidMount(){
    room_data.getRoomData();
  }

  render(){
    return(
     
      
      <Row>
      <Col>
        <Card small className="mb-4">
          <CardHeader className="border-bottom">
            <h6 className="m-0">Rooms</h6>
          </CardHeader>
          <CardBody className="p-0 pb-3 scroll_list-group">
          { (!this.props.isLoading && this.props.data.length>0) &&
            <table className="table mb-0">
              <thead className="bg-light">
                <tr>
                  <th scope="col" className="border-0">
                    #
                  </th>
                  <th scope="col" className="border-0">
                    Room No.
                  </th>
                  <th scope="col" className="border-0">
                    Room Name
                  </th>
                  <th scope="col" className="border-0">
                      Remove
                  </th>
                </tr>
              </thead>
              <tbody>
                    {
                      this.props.data.map((item,i)=>{
                                return (<Trow key={i} data={item} index={i+1}/>)
                      })
                    }
                
              </tbody>
            </table>
          }
          {
            (this.props.data.length===0 && (!this.props.isLoading)) &&
            <Message message={"No Room Data"} icon_name={'warning'} />
          }
          {
            (this.props.isLoading) &&
            <center>
                <AtomSpinner color={'blue'} />
            </center>
          }
          </CardBody>
        </Card>
      </Col>
    </Row>
  
    )
  }
}



class Trow extends React.Component{
  render(){
    return(
      <tr>
          <td>{this.props.index}</td>
          <td>{this.props.data.room_no}</td>
          <td>{this.props.data.room_name}</td>
          <td><DeleteButton onClick={()=>{room_data.removeRoomData(this.props.data.room_no);}}/></td>
    </tr>

    )
  }
}