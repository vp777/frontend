import React from 'react';
import RoomTable from './RoomTable';
import { Container, Row,Col,Button,Alert} from "shards-react";
import PageTitle from "../common/PageTitle";
import AddRoom from './AddRoom';
import SearchInp from '../common/SearchInp';
import AlertBox from '../AlertBox';
import room_data from '../../stores/roomStore';
import { Redirect } from "react-router-dom";
import login_data from '../../stores/loginStore';
import {view} from 'react-easy-state';

export default view(()=>{
	var isLoggedIn = login_data.isLoggedIn;
	return (isLoggedIn?<Room />:<Redirect to="/login" />)
	//return <Room />
})

class Room extends React.Component{
	

	

	render(){
		return(
			<div>
			<AlertBox />
			<Container fluid className="main-content-container px-4">
      		{/* Page Header */}
      		
      		<Row noGutters className="page-header py-4">
        			<PageTitle sm="4" title="Room" subtitle="Manage" className="text-sm-left" />
      		</Row>
      			<div className="pt-3"><AddRoom /></div>
			  	<Row>
			  			<Col md="4" sm="4"><div className="pt-3 mr-2">
							<SearchInp 
								fields={[{value:'room_name',name:"Room Name"},
										{value:"room_no",name:'Room no.'}]}
								setAttribute={(evt)=>{room_data.search_attribute=evt.target.value}}
								search={(evt)=>{room_data.search(evt.target.value)}}/>
					</div></Col>	
			   </Row>
			   		
					<RoomTable />
			</Container>
			</div>
		)
	}
}