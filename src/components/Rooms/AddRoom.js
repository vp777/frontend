import React from "react";
import {  Modal, ModalBody, ModalHeader,Col,Button} from "shards-react";
import Input from '../common/Input';
import room_data from '../../stores/roomStore';

function setRoomNo(no){
	room_data.setRoomNo(no);
}

function setRoomName(name){
  room_data.setRoomName(name);
}

function addRoom(){
	room_data.addRoom();
}

export default class AddRoom extends React.Component{
       constructor(props) {
            super(props);
            this.state = { open: false };
            this.toggle = this.toggle.bind(this);
        }

    
      toggle() {
        this.setState({
          open: !this.state.open
        });
      }
    
      render() {
        const { open } = this.state;
        return (
          <div>
            <Button type="primary" onClick={this.toggle}>Add New Room</Button>
            <Modal open={open} toggle={this.toggle}>
              <ModalHeader>
                    <b>Add Room</b>     
            </ModalHeader>
              <ModalBody>
              
                <Input size="8" name="Room No." type="text" onChange={(evt)=>{setRoomNo(evt.target.value)}} />
                <Input size="8" name="Room Name" type="text" onChange={(evt)=>{setRoomName(evt.target.value)}} />
                <Col size="8" className={"pt-3"}><Button theme="primary" onClick={()=>{addRoom();this.toggle();}}>Add Room</Button></Col>
              </ModalBody>
            </Modal>
          </div>
        );
      }
}