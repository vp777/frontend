import React from "react";
import {  Row, Col, Card, CardHeader, CardBody ,Button} from "shards-react";
import ViewButton from '../common/ViewButton';
import DeleteButton from '../common/DeleteButton';
import { AtomSpinner } from 'react-epic-spinners';
import {view} from 'react-easy-state';
import Message from '../Message';
import sub_data from '../../stores/subjectStore';
import '../scroll.css';


export default view(()=>{
  
  return <SubjectTable isLoading={sub_data.isLoading} data={sub_data.viewData}/>
})

class SubjectTable extends React.Component{

  componentDidMount(){
    sub_data.getData();
  }

  render(){
    return(
      <Row>
      <Col>
        <Card small className="mb-4">
          <CardHeader className="border-bottom">
            <h6 className="m-0">Subjects</h6>
          </CardHeader>
          <CardBody className="p-0 pb-3 scroll_list-group">
            {
              
              (!this.props.isLoading && (this.props.data.length>0)) &&
            
            <table className="table mb-0">
              <thead className="bg-light">
                <tr>
                  <th scope="col" className="border-0">
                    #
                  </th>
                  <th scope="col" className="border-0">
                    SubjectID
                  </th>
                  <th scope="col" className="border-0">
                    Subject Name
                  </th>
                  <th scope="col" className="border-0">
                      ClassID
                  </th>
                  <th scope="col" className="border-0">
                      RoomID
                  </th>
                  <th scope="col" className="border-0">
                      TeacherID
                  </th>
                  <th scope="col" className="border-0">
                    No. of Classes / week
                  </th>
                  <th scope="col" className="border-0">
                      Duration
                  </th>
                  <th scope="col" className="border-0">
                      Subject Type
                  </th>
                  <th scope="col" className="border-0">
                      Remove
                  </th>
                </tr>
              </thead>
              <tbody>

                {
                      this.props.data.map((item,i)=>{
                                return (<Trow key={i} data={item} index={i+1}/>)
                      })
                }


                
              </tbody>
            </table>
            }
            {
              this.props.isLoading && 
              <center>
                  <AtomSpinner color={'blue'} />
              </center>
            }
             {
                ((this.props.data.length===0) && !this.props.isLoading ) &&
                <Message message={"Kindly Add a Subject"} icon_name={'call_missed'} />
            }
          </CardBody>
        </Card>
      </Col>
    </Row>
  
    )
  }
}





class Trow extends React.Component{
  render(){
    return(
      <tr>
           <td>{this.props.index}</td>
          <td>{this.props.data.subject_id}</td>
          <td>{this.props.data.subject_name}</td>
          <td>{this.props.data.class_id}</td>
          <td>{this.props.data.room_id}</td>
          <td>{this.props.data.teacher_id}</td>
          <td>{this.props.data.no_of_classes_per_week}</td>
          <td>{this.props.data.duration_of_one_class}</td>
          <td>{this.props.data.subject_type}</td>
          <td><DeleteButton onClick={()=>{sub_data.removeSubject(this.props.data.subject_id)}}/></td>
      </tr>
    )
  }
}