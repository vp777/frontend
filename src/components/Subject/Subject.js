import React from 'react';
import SubjectTable from './SubjectTable';
import firebase from '../../firebase';
import { Container, Row,Col,Button} from "shards-react";
import PageTitle from "../common/PageTitle";
import AddSubject from './AddSubject';
import SearchInp from '../common/SearchInp';
import AlertBox from '../AlertBox';
import sub_data from '../../stores/subjectStore';
import {view} from 'react-easy-state';
import { Redirect } from "react-router-dom";
import login_data from '../../stores/loginStore';

export default view(()=>{
	var isLoggedIn = login_data.isLoggedIn;
	return (isLoggedIn?<Subject />:<Redirect to="/login" />)
	//return <Subject />
})

class Subject extends React.Component{
	constructor(props){
		super(props);
		this.state = {addSubject:false};
		this.addSubject = this.addSubject.bind(this);
		this.removeSubject = this.removeSubject.bind(this);
	}

	addSubject(){
		this.setState({addSubject:true});
	}

	removeSubject(){
		this.setState({addSubject:false});
	}

	render(){
		return(
			<div>
				<AlertBox />
			<Container fluid className="main-content-container px-4">
      		{/* Page Header */}
      		<Row noGutters className="page-header py-4">
        			<PageTitle sm="4" title="Subject" subtitle="Manage" className="text-sm-left" />
      		</Row>
      			<div className="pt-3"><Button theme="primary" size="sm" onClick={this.addSubject}>Add New Subject</Button></div>
			  	<Row>
			  			<Col md="4" sm="4">
						<div className="pt-3 mr-2">
							  	<SearchInp fields={[
										  {value:'subject_name',name:"Subject Name"},
										  {value:"subject_id",name:'Subject ID'},
										  {value:"teacher_id",name:'Teacher ID'},
										  {value:"class_id",name:'Class ID'},
										  {value:"room_id",name:'Room ID'},
										  ]}
										  setAttribute={(evt)=>{sub_data.search_attribute=evt.target.value}}
										  search={(evt)=>{sub_data.search(evt.target.value)}}
										/>
						</div>
						</Col>	
			   </Row>
			   		<AddSubject close={this.removeSubject} open={this.state.addSubject} />
					<SubjectTable />
			</Container>
			</div>
		)
	}
}