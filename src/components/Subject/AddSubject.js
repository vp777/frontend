import React from "react";
import {  Modal, ModalBody, ModalHeader,Col,Button,FormSelect,Row, Form } from "shards-react";
import Input from '../common/Input';
import ClassSelect from '../ClassSelect';
import TeacherSelect from '../SelectTeacher';
import RoomSelect from '../SelectRoom';
import sub_data from '../../stores/subjectStore';
import {view } from 'react-easy-state';

export default view((props)=>{
  return <AddSubject {...props} isLoading={sub_data.isLoading}/>;
})


class AddSubject extends React.Component{
       constructor(props) {
            super(props);
            this.state = { open: false };
            this.toggle = this.toggle.bind(this);
            this.addSubject = this.addSubject.bind(this);
        }

        componentDidMount(){
            this.setState({open:this.props.open});
        }

        componentDidUpdate(prevProps){
            if(prevProps.open!==this.props.open){
                this.setState({open:this.props.open});
            }
        }

        addSubject(){
            sub_data.addSubject();
            this.toggle();
        }
    
      toggle() {
        this.setState({
          open: !this.state.open
        });

        this.props.close();
      }
    
      render() {
        const { open } = this.state;
        return (
          <div>
            <Modal open={open} toggle={this.toggle}>
              <ModalHeader>
                    <b>Add Subject</b>     
            </ModalHeader>
              <ModalBody>
                <Row>
                    <Input size="6" name="SubjectID" type="text" onChange={(evt)=>{sub_data.subject_id = evt.target.value;}} />
                    <Input size="6" name="Subject Name" type="text" onChange={(evt)=>{sub_data.subject_name = evt.target.value;}} />
                </Row>
                <Row>
                    <Input size="6" name="No of classes/week" type="number" onChange={(evt)=>{sub_data.no_of_classes = evt.target.value;}} />
                    <Input size="6" name="Duration of Class(Hrs)" type="number" onChange={(evt)=>{sub_data.duration_of_class = evt.target.value;}} /> 
                </Row>
                <Row>
                      <SelectClass onChange={(evt)=>{sub_data.class_id = evt.target.value;}}/> 
                      <SelectTeacher onChange={(evt)=>{sub_data.teacher_id = evt.target.value;}}/>
                </Row>
                <Row>
                    <SelectRoom onChange={(evt)=>{sub_data.room_id = evt.target.value;}}/>
                    <SelectSubjectType onChange={(evt)=>{sub_data.subject_type = evt.target.value;}}/>
                </Row>
                <Col size="8" className={"pt-3"}><Button theme="primary" onClick={this.addSubject}>Add Subject</Button></Col>
              </ModalBody>
            </Modal>
          </div>
        );
      }
}

class SelectSubjectType extends React.Component{
  render(){
    return (
      <Col md={"6"} className="form-group">
        <label>{"Subject Type"}</label>
        <FormSelect onChange={this.props.onChange}>
            <option value={"theory"}>Theory</option>
            <option value={"practical"}>Practical</option>
        </FormSelect>
      </Col>
    )
  }
}

class SelectClass extends React.Component{
  render(){
    return(
    <Col md={"6"} className="form-group">
      <label >{"Class"}</label>
      <ClassSelect onChange={this.props.onChange} />
    </Col>
    )
  }
}

class SelectRoom extends React.Component{
  render(){
    return(
    <Col md={"6"} className="form-group">
      <label >{"Room"}</label>
      <RoomSelect onChange={this.props.onChange}/>
    </Col>
    )
  }
}

class SelectTeacher extends React.Component{
  render(){
    return(
    <Col md={"6"} className="form-group">
      <label >{"Teacher"}</label>
      <TeacherSelect onChange={this.props.onChange}/>
    </Col>
    )
  }
}