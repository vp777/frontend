import React from 'react';
import {Col,Card,CardHeader,CardBody,Row,Button} from 'shards-react';
import {AtomSpinner} from 'react-epic-spinners';
import ClassList from '../ClassSelect';
import class_timetable_data from '../../stores/TimeTableStore';
import {view} from 'react-easy-state';
import Message from '../Message';

export default view(()=>{
    return <TimeTableView  class_id={class_timetable_data.class_id} isLoading={class_timetable_data.isLoading} data={class_timetable_data.day_table} />;
})

class TimeTableView extends React.Component{

    render(){
        const days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        return(
            <Col>
                <Card small className="mb-4"  >
                    <CardHeader className="border-bottom">
                            <h6 className="m-0">TimeTable</h6>
                    </CardHeader>
        
                    <CardBody className="pt-0 scroll_list-group">
                        
                        <Row className="border-bottom py-2 bg-light">   
                            <Col sm="4" className="d-flex mb-2 mb-sm-0">
                                <ClassList onChange={(evt)=>{class_timetable_data.setClassID(evt.target.value)}}/>
                            </Col>
                            {
                                (this.props.data.length>0) &&
				                <Col md={4} lg={4} sm={4} xs={4}>
					                <Button theme="primary" onClick={()=>{
                                        window.open(`timetableprint.html?data=${JSON.stringify(this.props.data)}`,"_blank");
                                    }}>Print</Button>
				                </Col>
                            }
                        </Row>

                    {

                        ((!this.props.isLoading) && (this.props.data.length>0) && (!this.props.isLoading) ) &&
                        
                        <table className="table mb-0 ">
                            <thead className="bg-light">
                            
                            <tr>
                                <th scope="col" className="border-0">
                                    #
                                </th>
                                {
                                    this.props.data.map((item,i)=>{
                                        return <th scope="col" className="border-0">{item["start_time"]}</th>
                                    })
                                }
                                
                            </tr>

                            </thead>
                            
                            <tbody>
                                
                            
                            <tr>
                                <td><b>Monday</b></td>
                                {
                                    this.props.data.map((item,i)=>{
                                        return (<td>{`${item["subject_name"]} (${item["name"]}) ${item["room_no"]} ${item["room_name"]}` }</td>)
                                    })
                                }
                            </tr>
                            <tr>
                                <td><b>Tuesday</b></td>
                                {
                                    this.props.data.map((item,i)=>{
                                        return (<td>{`${item["subject_name"]} (${item["name"]}) ${item["room_no"]} ${item["room_name"]}` }</td>)
                                    })
                                }
                            </tr>
                            <tr>
                                <td><b>Wednesday</b></td>
                                {
                                    this.props.data.map((item,i)=>{
                                        return (<td>{`${item["subject_name"]} (${item["name"]}) ${item["room_no"]} ${item["room_name"]}` }</td>)
                                    })
                                }
                            </tr>
                            <tr>
                                <td><b>Thursday</b></td>
                                {
                                    this.props.data.map((item,i)=>{
                                        return (<td>{`${item["subject_name"]} (${item["name"]}) ${item["room_no"]} ${item["room_name"]}` }</td>)
                                    })
                                }
                            </tr>
                            <tr>
                                <td><b>Friday</b></td>
                                {
                                    this.props.data.map((item,i)=>{
                                        return (<td>{`${item["subject_name"]} (${item["name"]}) ${item["room_no"]} ${item["room_name"]}` }</td>)
                                    })
                                }
                            </tr>
                            <tr>
                                <td><b>Saturday</b></td>
                                {
                                    this.props.data.map((item,i)=>{
                                        return (<td>{`${item["subject_name"]} (${item["name"]}) ${item["room_no"]} ${item["room_name"]}` }</td>)
                                    })
                                }
                            </tr>
                            
               
                            </tbody>
                        </table>

                    }
                    {
                        (this.props.isLoading) &&
                        <center>
                                 <AtomSpinner color={'blue'} />
                        </center>
                    }
                    {
                        ((this.props.class_id===undefined) && (!this.props.isLoading)) &&
                        <Message icon_name={"call_missed"} message={"Please Select a Class"} />
                    }
                    {
                        ((this.props.data.length===0) && (this.props.class_id!==undefined) && (!this.props.isLoading) ) &&
                        <Message icon_name={"airline_seat_individual_suite"} message={"You Have No Class Today "} />
                    }
                 </CardBody>
             </Card>
            </Col>
        
        )
    }
}


function Trow(props){
    return(
         <tr>
                <td>{props.index}</td>
                <td>{props.data.subject_name}</td>
                <td>{props.data.room_name+"("+props.data.room_no+")"}</td>
                <td>{props.data.start_time}</td>
                <td>{props.data.end_time}</td>
                <td>{props.data.name}</td>
        </tr>
    )
}

