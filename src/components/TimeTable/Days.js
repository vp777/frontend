
import React,{Component} from 'react';
import {  Row, Button, } from "shards-react";
import timetable_data from '../../stores/TimeTableStore';



export default class Days extends Component{

	constructor(props) {
	  super(props);
	
	  this.state = {day_selected:'Monday'};

	  this.onLeftClick      = this.onLeftClick.bind(this);
      this.onRightClick      = this.onRightClick.bind(this);

       this.days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

	}

	componentDidMount(){
		//this.props.update_day(this.state.day_selected);
	}


	onLeftClick = () =>{

	  var day = undefined;

      for(var i=0;i<this.days.length;i++){
          if(this.state.day_selected === this.days[i]){
              if(i!==0){
                    day = this.days[i-1];
                    timetable_data.setDay(day.toLowerCase());
                   this.setState({day_selected:day});
              }
              else{
              	  day = this.days[this.days.length-1];
                  timetable_data.setDay(day.toLowerCase());
                  this.setState({day_selected:day});
              }
          }
      }

     // this.props.update_day(day);

  }


  onRightClick = () =>{


  	var day = undefined;

      for(var i=0;i<this.days.length;i++){
          if(this.state.day_selected === this.days[i]){
              if(i!==(this.days.length-1)){
              	  day = this.days[i+1];
                  timetable_data.setDay(day.toLowerCase());
                  this.setState({day_selected:day});
              }
              else{
              	  day = this.days[0];
                  timetable_data.setDay(day.toLowerCase());
                  this.setState({day_selected:day});
              }
          }
      }

     // this.props.update_day(day);

  }

	render(){
		return(

			  <Row style={{marginLeft:'3%',marginBottom: '3%'}}>
                  
                  <Button theme="info" size="sm" style={{fontSize:20}} onClick={this.onLeftClick}>
                        <i class="material-icons">keyboard_arrow_left</i>
                  </Button>
                  
                  <div style={{marginLeft: '2%',marginTop: '0.5%',textAlign:'center',fontSize: 20,fontWeight: 'bold',width:'9%' }}>
                      {this.state.day_selected}
                  </div>
                  
                  <Button style={{marginLeft: '2%',fontSize:20}} theme="info" size="sm" onClick={this.onRightClick}>
                          <i class="material-icons">keyboard_arrow_right</i>
                  </Button>

              </Row>
          )
	}
}