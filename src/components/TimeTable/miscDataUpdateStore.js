import {store} from 'react-easy-state';
import alert from './../../stores/Alert';
import host from '../../stores/host';
import databaseSelectionStore from '../databaseSelectionStore';
const request = require('request');

const miscDataUpdateStore = store({

    isLoading : false,

    miscStartDate : null,

    updateStartDate : null,

    getMiscStartDate(){
        miscDataUpdateStore.isLoading = true;

        const url = `${host}getMiscStartDate?departmentName=${databaseSelectionStore.selectedDepartment}`;
        request.get(url,function(err,res,body){
            if(err){
                miscDataUpdateStore.isLoading = false;
                alert.showMessage(err,"danger");
                return;
            }

            var result = JSON.parse(body);
            console.log(result);

            if(result["status"]==="success"){
                miscDataUpdateStore.miscStartDate = new Date(result["start_date"]);
                miscDataUpdateStore.isLoading = false;
                return;
            }
            else{
                miscDataUpdateStore.isLoading = false;
                return;
            }
        })
    },


    updateMiscStartDate(){
        
        if(!miscDataUpdateStore.updateStartDate){
            alert.showMessage("Misc start date is not valid");
            return;
        }
        
        miscDataUpdateStore.isLoading = true;  
    
        var newDate = new Date(miscDataUpdateStore.updateStartDate);

        var dateString = newDate.getDay() +"-" + (newDate.getMonth()+1) + "-" + (newDate.getFullYear());

        const url = `${host}updateMiscStartDate?departmentName=${databaseSelectionStore.selectedDepartment}&start_date=${dateString}`;
        request.get(url,function(err,res,body){
            if(err){
                miscDataUpdateStore.isLoading = false;
                alert.showMessage(err,"danger");
                return;
            }

            var result = JSON.parse(body);
            console.log(result);

            if(result["status"]==="success"){
                alert.showMessage("Start date updated succesfully");
                miscDataUpdateStore.isLoading = false;
                miscDataUpdateStore.getMiscStartDate();
                return;
            }
            else{
                miscDataUpdateStore.isLoading = false;
                return;
            }
        })
    }

})  

export default miscDataUpdateStore;