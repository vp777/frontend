import React, { useEffect } from 'react';
import { Container, FormInput, Row,Col, Button} from "shards-react";
import PageTitle from "../common/PageTitle";
import TimeTableView from './TimeTableView';
import AlertBox from '../AlertBox';
import {view} from 'react-easy-state';
import { Redirect } from "react-router-dom";
import login_data from '../../stores/loginStore';
import { AtomSpinner } from 'react-epic-spinners';
import miscDataUpdateStore from './miscDataUpdateStore';

export default view(()=>{
	useEffect(()=>{
		if(miscDataUpdateStore.miscStartDate===null){
			miscDataUpdateStore.getMiscStartDate();
		}
	})
	var isLoggedIn = login_data.isLoggedIn;
	return (isLoggedIn?<TimeTable isMiscLoading={miscDataUpdateStore.isLoading} miscStartDate={miscDataUpdateStore.miscStartDate} />:<Redirect to="/login" />)
})


class TimeTable extends React.Component{
	constructor(props){
		super(props);
		this.state = {addTimeTable:false};

		this.addTimeTable = this.addTimeTable.bind(this);
		this.removeTimeTable = this.removeTimeTable.bind(this);
	}

	

	addTimeTable(){
		this.setState({addTimeTable:true});
	}

	removeTimeTable(){
		this.setState({addTimeTable:false});
	}

	

	render(){
		return(
			<div>
			<AlertBox />

			<Container fluid className="main-content-container px-4">
      		{/* Page Header */}
      		<Row noGutters className="page-header py-4">
        			<PageTitle sm="4" title="TimeTable" subtitle="Manage" className="text-sm-left" />
      		</Row>
			<br />
			<Col>
				<Row>
					<Col md={4} lg={4} sm={4} xs={4}>
						{
							this.props.isMiscLoading &&
							<AtomSpinner color={"blue"}/>
						}
						{
							<>
								<label>Start Date</label>
								<h6>{this.props.miscStartDate===null?"Waiting": this.props.miscStartDate.toDateString()}</h6>
								<FormInput type="date"  placeholder="Start Date" onChange={(evt)=>{miscDataUpdateStore.updateStartDate = evt.target.value}}/>
								<Button theme="primary" style={{marginTop:"5px"}} onClick={()=>{miscDataUpdateStore.updateMiscStartDate()}}>Update</Button>
							</>
						}
					</Col>
					
				</Row>	
				
			</Col>
			<br />
			<TimeTableView />
					
			</Container>
			</div>
		)
	}
}
