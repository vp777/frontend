import React from "react";
import { 
  		FormSelect ,
	} from "shards-react";
import { view } from 'react-easy-state';
import teacher_data from '../stores/TeacherStore';



export default view((props)=>{     
                                  return <SelectTeacher data={teacher_data.data} 
                                                   	  isLoading={teacher_data.isLoading}
                                                   	  onChange = {props.onChange}
                                          />
                      });

class SelectTeacher extends React.Component{
  
  componentDidMount(){    
            console.log("Running Teacher");
            teacher_data.getData();
	}


  render(){
    return (     
     <FormSelect onChange ={this.props.onChange}>
         
        <option value={undefined}>{this.props.isLoading?"Loading...":"Select Teacher"}</option>
        {

               this.props.data.map((item,i)=>{
                         return (<option key={i} value={item.tid}>{item.name}</option>)
                })
              
        } 
      </FormSelect>
  )
  }
}

