import React from "react";
import {  Modal, ModalBody, ModalHeader,Col,Button,FormSelect,Row } from "shards-react";
import Input from '../common/Input';
import SelectRoles from '../SelectRoles';
import user_data from '../../stores/UserStore';

export default class AddUser extends React.Component{
    constructor(props) {
        super(props);
        this.state = { open: false };
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
          open: !this.state.open
        });
    }
    render(){
        const { open } = this.state;
        return(
            <div>
            <Modal open={open} toggle={this.toggle}>
              <ModalHeader>
                    <b>Add User</b>     
            </ModalHeader>
              <ModalBody>
                <Row>
                    <Input  name="User Name" type="text" onChange={(evt)=>{user_data.user_name=evt.target.value}} />
                    <Input  name="User Id" type="text" onChange={(evt)=>{user_data.user_id = evt.target.value}} />
                </Row>
                <Row>
                    <Input  name="Password" type="password" onChange={(evt)=>{user_data.password = evt.target.value}} />
                    <Input  name="Confirm Password" type="password" onChange={(evt)=>{user_data.confirm_password = evt.target.value}} /> 
                </Row>
                <Row>  
                    <Col lg="6" md="6" sm="12"> 
                            <label>Role</label>
                            <SelectRoles onChange={(evt)=>{user_data.role_id = evt.target.value}} />
                    </Col>
                </Row>
                <Col  className={"pt-3"}><Button theme="primary" onClick={()=>{user_data.addUser();this.toggle()}}>Add User</Button></Col>
              </ModalBody>
            </Modal>
            <Button onClick={this.toggle}>Add New User</Button>
          </div>
        )
    }
}