import React from 'react';
import { Container, Row,Button} from "shards-react";
import PageTitle from "../common/PageTitle";
import AlertBox from '../AlertBox';
import AddUser from './AddUser';
import UserTable from './UserTable';
import {view} from 'react-easy-state';
import { Redirect } from "react-router-dom";
import login_data from '../../stores/loginStore';

export default view(()=>{
	var isLoggedIn = login_data.isLoggedIn;
	return (isLoggedIn?<User />:<Redirect to="/login" />)
	//return <User />;
})

class User extends React.Component{
    render(){
        return(
            <div>
			<AlertBox />

			<Container fluid className="main-content-container px-4">
      		{/* Page Header */}
      		<Row noGutters className="page-header py-4">
        			<PageTitle sm="4" title="Users" subtitle="Manage" className="text-sm-left" />
      		</Row>

      		<div className="p-3"><AddUser /></div>

              <UserTable />
					
			</Container>
			</div>
        )
    }
}