import React from 'react';
import {Col,Card,CardHeader,CardBody,Row,Button} from 'shards-react';
import {AtomSpinner} from 'react-epic-spinners';
import {view} from 'react-easy-state';
import Message from '../Message';
import user_data from '../../stores/UserStore';

export default view(()=>{
    return <UserTable  isLoading={user_data.isLoading} data={user_data.data} />;
})


class UserTable extends React.Component{

    componentWillMount(){
        user_data.getData();
    }

    render(){
        return(
            <Col>
                <Card small className="mb-4"  >
                    <CardHeader className="border-bottom">
                            <h6 className="m-0">Users</h6>
                    </CardHeader>
        
                    <CardBody className="pt-0 scroll_list-group">

                    {

                        ((!this.props.isLoading) && (this.props.data.length>0) ) &&
                        
                        <table className="table mb-0 ">
                            <thead className="bg-light">
                            
                            <tr>
                                <th scope="col" className="border-0">
                                    #
                                </th>
                                <th scope="col" className="border-0">
                                    User ID
                                </th>
                                <th scope="col" className="border-0">
                                    User Name
                                </th>
                                <th scope="col" className="border-0">
                                    Role
                                </th>
                                <th scope="col" className="border-0">
                                    Delete
                                </th>
                                
                            </tr>

                            </thead>
                            
                            <tbody>
                            {
                                this.props.data.map((item,i)=>{
                                    return (<Trow key={i} data={item} index={i+1}/>)
                                })
                            }
               
                            </tbody>
                        </table>

                    }
                    {
                        (this.props.isLoading) &&
                        <center>
                                 <AtomSpinner color={'blue'} />
                        </center>
                    }
                    {
                        ((this.props.data.length===0)  && (!this.props.isLoading) ) &&
                        <Message icon_name={"warning"} message={"No Users Added "} />
                    }
                 </CardBody>
             </Card>
            </Col>
        
        )
    }
}


function Trow(props){
    return(
         <tr>
                <td>{props.index}</td>
                <td>{props.data.user_id}</td>
                <td>{props.data.user_name}</td>
                <td>{props.data.role_name}</td>
                <td><Button theme="danger" onClick={()=>{user_data.delUser(props.data.user_id)}}>Delete</Button></td>
        </tr>
    )
}

