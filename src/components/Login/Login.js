import React from "react";
import { Container, Button,FormInput, FormSelect } from "shards-react";
import AlertBox from '../AlertBox';
import { view } from 'react-easy-state';
import login_data from '../../stores/loginStore';
import { AtomSpinner } from 'react-epic-spinners';
import { Redirect } from "react-router-dom";
import databaseSelectionStore from "../databaseSelectionStore";


export default view(()=>{
    var data = login_data;
    console.log(data);
    var isLoading = data.isLoading;
    var isLoggedIn = data.isLoggedIn; 

    console.log("Logged in ",isLoggedIn);
    return (isLoggedIn?<Redirect to="/teacher" />:<Login isLoading={isLoading} departmentNames={databaseSelectionStore.departmentNames}/>)
})

function setUserName(uname){
  login_data.username = uname;
}
function setPassword(password){
  login_data.password = password;
}

function login(){
  login_data.login();
}

class Login extends React.Component{
  render(){
    console.log("Department names = ",this.props.departmentNames);
    return(
      <div>
			<AlertBox />

  <Container fluid className="main-content-container px-4 pb-4">
    <div className="error">
      <div className="error__content" style={{backgroundColor: 'white'}}>
      {
        !this.props.isLoading &&
        <div className={"p-4"}>
        <h4 style={{marginTop: '5%'}}>MILITARY COLLEGE</h4>
        <FormInput type="text" placeholder="Username" style={{margin: '5%'}} onChange={(evt)=>{login_data.setUserName(evt.target.value)}}/>
        <FormInput type="password" placeholder="Password" style={{margin: '5%'}} onChange={(evt)=>{login_data.setPassword(evt.target.value)}}/>
        
        <FormSelect placeholder="Faculties" onChange={(e)=>{databaseSelectionStore.selectedDepartment = e.target.value}}>
          <option value={null}>Select</option>
          {
            this.props.departmentNames.map((val,index)=>{
              return <option>{val}</option>;
            })
          }
        </FormSelect>
        <br />
        <br />
        <Button size="md" style={{marginBottom: '5%'}} onClick={()=>{login()}}>Login</Button>
        </div>
      }
      {
        this.props.isLoading &&
        <AtomSpinner color="blue" style={{alignSelf: 'center',marginTop: '10%',marginBottom: '10%'}}/>
      }
      </div>
    </div>
  </Container>

  </div>

      )
  }
}

