import React from "react";
import { 
  		FormSelect ,
	} from "shards-react";
import { view } from 'react-easy-state';
import role_data from '../stores/RoleStore';

export default view((props)=>{     
    return <SelectRole data={role_data.data} 
                           isLoading={role_data.isLoading}
                           onChange = {props.onChange}
            />
});


class SelectRole extends React.Component{
	componentDidMount(){    
            console.log("Running Teacher");
            role_data.getData();
    }
    render(){
        return (     
         <FormSelect onChange ={this.props.onChange}>
             
            <option value={undefined}>{this.props.isLoading?"Loading...":"Select Role"}</option>
            {
    
                   this.props.data.map((item,i)=>{
                             return (<option key={i} value={item.role_id}>{item.role_name}</option>)
                    })
                  
            } 
          </FormSelect>
      )
      }
    }
    